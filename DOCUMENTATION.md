# Documentation

As seen on the README file, this repo comes with 3 scripts, ``datasetextender.py``, ``extend_huge.py`` and ``gui.py``.

``datasetextender.py`` and ``gui.py`` consists in expanding the dataset with a single transformations. And if you want to combine every pair of transformations in a single image you can run ``huge_extend.py`` but you wont be able to select the specific transformations to apply.

## How to create your own script


- [Class Extender()](#class-extender)
- [Extender.extend() method](#extenderextend-method)
  - [Transformation array constants](#transformation-array-constants)
  - [Arguments](#arguments)

### Class Extender()

In order to expand the dataset you will need to create an object of the Extender class, where you specify the array of the images dir, and optionally, select if you want to create an XML and/or TXT labeling files.

ex:
```python
input_folder = #folder where all images and its labels are stored
images = glob.glob(input_folder + "/*.jpg")
images.sort(key=natural_keys)

dataset = Extender(images, xml_output=True, txt_output=True)
```

### Extender.extend() method

Once the object is instanciated, you can now extend the dataset with the ```Extender.extend()``` method

ex:
```python
output_folder = #output folder
dataset.extend(output_folder, "BW", "bw", [Extender.BLACK_AND_WHITE])
```

```Extender.extend()``` recieves 5 parameters:

- **Output folder**
- **Folder tag**: tag at the end of the folder name
- **File tag**: tag at the beggining of the file name
- **Multiple applied transformations array**: array with the transformations you are willing to apply at the same time
- **Args**: transformations options

The transformations array is defined as for example:
```python
extenders = [
    Extender.BNDBOX_MIRROR,
    Extender.BNDBOX_NOISE,
    Extender.RAIN,
    Extender.BLUR
]

dataset.extend(output_folder, "test", "t", extenders)
```
This will create a folder with the "-test" tag where each file has a "t_" tag, and each image's boundbox was mirrored and noise was applied, for then to apply a rain and blur effect.

![](https://iili.io/QqDRt9.png)

Also, the method recieves extra args to modify certain transformations options, for example:
```python
dataset.extend(output_folder, "test", "t", [Extender.ROTATE], rotate=45) 
#Rotates the image 45º
```

#

#### Transformation array constants
```js
BILATERAL
BLACK_AND_WHITE
BLUR
BNDBOX_BILATERAL
BNDBOX_BLACK_AND_WHITE
BNDBOX_BLUR
BNDBOX_BRIGHTNESS_CONTRAST
BNDBOX_CARTOON
BNDBOX_CLAHE
BNDBOX_COLOR_INVERT
BNDBOX_COLOR_MAP
BNDBOX_DILATION
BNDBOX_EROSION
BNDBOX_MIRROR
BNDBOX_MOTION_BLUR
BNDBOX_NOISE
BNDBOX_POSTERIZE
BNDBOX_ROTATE
BNDBOX_SHARPEN
BNDBOX_SMOOTH
BNDBOX_SOLARIZE
BNDBOX_TRANSLATE
BRIGHTNESS_CONTRAST
CARTOON
CLAHE
COLOR_FILTER
COLOR_INVERT
COLOR_MAP
COLOR_PSEUDO
CUT_MIX
DILATION
EDGE
EQUALIZE
EROSION
MIRROR
MIXUP
MOSAIC
MOTION_BLUR
NOISE
OCCLUSION_HNS
OCCLUSION_MASK
OCCLUSION_RANDOM
PERSPECTIVE
POSTERIZE
RAIN
RESIZE
ROTATE
SCALE
SHARPEN
SMOOTH
SOLARIZE
TRANSLATE
```

#### Arguments

Map with each argument range as [min,max,step].

```python
"blur": [5,11,1]
"color": [0,11,1]
"dilation": [1,4,1]
"erosion": [1,4,1]
"mirror": [0,2,1]
"mosaic": [1,30,1]
"occlusion_mask": [1,100,1]
"posterize": [1,4,1]
"rotate": [0,360,1]
"scale": [0,2,1]
"smooth": [3,9,2] #Only odd values
```