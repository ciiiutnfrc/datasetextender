# DatasetExtender

A dataset extender coded in python

- [DatasetExtender](#datasetextender)
  - [Compatibility](#compatibility)
  - [Functionalities](#functionalities)
  - [Documentation](#documentation)
- [Usage](#usage)
  - [GUI](#gui)
    - [GUI Installation](#gui-installation)
    - [GUI usage](#gui-usage)
  - [Terminal](#terminal)
    - [Terminal usage](#terminal-usage)
    - [Terminal Command List](#terminal-command-list)
  - [Massive extend](#massive-extend)


## Compatibility

This script is compatible with the following input dataset types:
- Pascal VOC (XML)
- YOLO (TXT)

All dataset label-types will be created on output.

## Functionalities

This script expands the dataset copying and modifying the following aspects of an image:

- Brightness and contrast filter
- Blur filter
- Boundbox transformations
- Color filters
- Cutmix images
- Dilation and erosion filters
- Edge algorithm (Canny)
- Mirroring (horizontal, vertical and mixed)
- Mixup images
- Noise and denoise filter
- Occlusion
- Perspective transformation
- Posterization
- Rotation (any angle, by default on commands 90º, 180º, 270º), translation
- Resizing
- Solarization
- Smoothing
- Zoom and cropping (on the labelled section)

## Documentation

If you want to create your own script using this code, you can read the documentation in [Documentation.md](DOCUMENTATION.md)

# Usage

## GUI

### GUI Installation
To use the GUI you'll need TKinter and some python libraries.
- #### Windows
    If you are on Windows it will generally come with python when you install it.

    Also, you'll need the libraries present on `requirements.txt`:
    ```
    python -m pip install -r requirements.txt
    ```

- #### Linux
    On Linux TKinter isn't part of the python installation so you'll need to install it, and also the requirements file.
    ```
    sudo apt install python3-tk
    python -m pip install -r requirements.txt
    ```

### GUI usage
The GUI is pretty straight forward.
- Select the input folder where the images are stored at
- Select the expansion to apply on the left frame and configure some options on the right frame if needed
- Finally select an output folder and click start.

![](https://iili.io/XbANGp.png)

## Terminal

### Terminal usage
With this repo clonned, you should be able to run the following command line

```
pyhon datasetextender.py --input ../imagenesPrueba/train -rx 45 -rd -zh -b -noi -v
```
This should create a new folder called `"/output"` in the root folder `../imagenesPrueba` with subfolders of each transformation, in this case: 45º and 270º rotation, horizontal zoom, brightness and contrast filter, noise filter, and vertical mirroring.

After tranformation, your folder tree should look something like this

```
├── imagenesPrueba
│   ├── output
│   │   ├── train-270
│   │   ├── train-45
│   │   ├── train-BR-CT
│   │   ├── train-CROPH
│   │   ├── train-NOI
│   │   └── train-VER
│   ├── test
│   └── train
```

### Terminal Command List

```
  -h, --help            Show this help message and exit
  --input INPUT_FOLDER  Input folder
  --output OUTPUT_FOLDER
                        Output folder (<input>/output folder by default)
  -a, --all             Apply all transformations *.
  -bbt, --bndboxtr      Apply boundbox transformations.
  -bc, --brightness         Add brightness & contrast
  -bf, --bilateral      Apply denoising filter.
  -bl, --blur           Apply blur.
  -bw                   Convert images to black and white.
  -bwf, --bw-all-tr     Apply all transformations to black and white images.
  -ca, --cartoon        Apply cartoon filtering.
  -cl, --clahe          Apply clahe filtering.
  -ci, --color-invert   Invert colors.
  -co, --color-map      Modify color mapping.
  -cf, --color-filter   Apply color filter.
  -cp, --color-pseudo   Apply pseudo-color filter.
  -cm, --cutmix         Apply cut-mix.
  -di, --dilate         Apply dilation.
  -eq, --equalize       Apply color equalization.
  -ed, --edge           Apply Canny's edge algorithm.
  -er, --erode          Apply erosion.
  -mb, --motion-blur    Apply motion blur.
  -mh, --horizontal     Apply horizontal mirroring
  -mhv, --hor-ver       Apply mix mirroring (horizontal and vertical)
  -mv, --vertical       Apply vertical mirroring
  -mu, --mixup          Apply image mixup.
  -mo, --mosaic         Apply mosaic transformation.
  -noi, --noise         Add noise
  -om <OCC_MSK>, --occlusion-msk <OCC_MSK>
                        Add mask occlusion, choose its <size>
  -oh, --occlusion-hns  Add multiple random occlusion boxes
  -or, --occlusion-rnd  Add an occlusion box in the space of interest
  -p, --perspective     Apply a random perspective transform to the image
  -pz, --posterize      Apply image posterization.
  -ra, --rain           Apply rain filter.
  -rn, --rotnov         Rotate 90 degrees.
  -rc, --rotcien        Rotate 180 degrees.
  -rd, --rotdos         Rotate 270 degrees.
  -rx <CONV_ROTX>, --rotx <CONV_ROTX>
                        Rotate <X> degrees.
  -rmx MULT_ROT, --rot-mult MULT_ROT
                        Apply multiple rotation, rotating N times each 360/Nº *.
  -s, --resize          Resize the image without changing its dimensions.
  -sh, --sharpen        Apply sharpen filtering.
  -so, --solarize       Apply solarization
  -sm, --smooth         Apply smoothing.
  -tr, --translate      Apply image translation.
  -z, --zoom            Apply zoom (scaling) and crop image
  -zh, --zoomh          Apply horizontal scaling and crop image
  -zv, --zoomv          Apply vertical scaling and crop image
  ``` 
 <p style='text-align: right;'> <font size="1"> -a doesn't apply transformations that require an input like -rx * <br>
 -rmx doesn't save rotations of 0 or 360º *</font> </p>

## Massive extend

If you want, you can massively (x1300) extend the dataset. It extends for each type of transformation, and transformation pair. Be ware that the output folder size will be huge, the script calculates the size before running the code so you can see the expected output size.

To run this code you should use the following command:
```
pyhon extend_huge.py --input <input_folder> --output <output_folder>
```
 <p style='text-align: right;'> <font size="1"> output folder is optional *</font> </p>