#! /usr/bin/env python 
# -*- coding: utf-8 -*- 
# vim:fenc=utf-8 
# 
# Copyright © 2019 Ciii UTN-FRC  <dgonzalezdondo@frc.utn.edu.ar> 
# Copyright © 2022 Ciii UTN-FRC  <agus.req@gmail.com> 
# 
# Distributed under terms of the GPLv3 license.

import os
from argparse import ArgumentParser
from optparse import OptionParser
import glob 
import re
from extenders import Extender

# Arguments

parser = ArgumentParser(description='Creates a new dataset from an existing one')
parser.add_argument( "--input", dest="input_folder", action="store", type=str, required=True, help="Input folder" )
parser.add_argument( "--output", dest="output_folder", action="store", type=str, required=False, help="Output folder (default folder: <input>/output)" )
parser.add_argument('-a', '--all', dest='all', action="store_true",default= False, help=" Apply all transformations.")
parser.add_argument('-bbt', '--bndboxtr', dest='bbt',action="store_true",default= False, help=" Apply boundbox transformations.")
parser.add_argument('-bc', '--brightness', dest='acbrillo',action="store_true",default= False, help=" Add brightness & contrast.")
parser.add_argument('-bf', '--bilateral', dest='bil_filter',action="store_true",default= False, help=" Apply bilateral denoising filter.")
parser.add_argument('-bl', '--blur', dest='add_blur',action="store_true",default= False, help=" Apply blur.")
parser.add_argument('-bw', dest='bw',action="store_true",default= False, help=" Convert images to black and white.")
parser.add_argument('-bwf', '--bw-all-tr', dest='bw_full',action="store_true",default= False, help=" Apply all transformations to black and white images.")
parser.add_argument('-ca', '--cartoon', dest='cartoon',action="store_true",default= False, help=" Apply cartoon filtering.")
parser.add_argument('-cl', '--clahe', dest='clahe',action="store_true",default= False, help=" Apply clahe filtering.")
parser.add_argument('-ci', '--color-invert', dest='color_invert',action="store_true",default= False, help=" Invert colors.")
parser.add_argument('-co', '--color-map', dest='mix_color',action="store_true",default= False, help=" Modify color mapping.")
parser.add_argument('-cf', '--color-filter', dest='color_filter',action="store_true",default= False, help=" Apply color filter.")
parser.add_argument('-cp', '--color-pseudo', dest='color_pseudo',action="store_true",default= False, help=" Apply pseudo-color filter.")
parser.add_argument('-cm', '--cutmix', dest='cut_mix',action="store_true",default= False, help=" Apply cut-mix.")
parser.add_argument('-di', '--dilate', dest='dilation',action="store_true",default= False, help=" Apply image dilation.")
parser.add_argument('-eq', '--equalize', dest='equalize',action="store_true",default= False, help="Apply color equalization.")
parser.add_argument('-ed', '--edge', dest='edge',action="store_true",default= False, help=" Apply Canny's edge algorithm.")
parser.add_argument('-er', '--erode', dest='erosion',action="store_true",default= False, help=" Apply image erosion.")
parser.add_argument('-mb', '--motion-blur', dest='motion_blur', action="store_true",default= False, help=" Apply motion blur.")
parser.add_argument('-mh', '--horizontal', dest='conv_hor', action="store_true",default= False, help=" Apply horizontal mirroring.")
parser.add_argument('-mhv', '--hor-ver', dest='conv_hor_ver', action="store_true",default= False, help=" Apply mix mirroring (horizontal and vertical).")
parser.add_argument('-mv', '--vertical', dest='conv_ver', action="store_true",default= False, help=" Apply vertical mirroring.")
parser.add_argument('-mu', '--mixup', dest='mixup', action="store_true",default= False, help=" Apply image mixup.")
parser.add_argument('-mo', '--mosaic', dest='mosaico', action="store_true",default= False, help=" Apply mosaic transformation.")
parser.add_argument('-noi', '--noise', dest='agg_noise', action="store_true",default= False, help=" Add noise.")
parser.add_argument('-om', '--occlusion-msk', dest='occ_msk', action="store", type=int,default= False, help=" Add mask occlusion, choose its <size> (0:random).")
parser.add_argument('-oh', '--occlusion-hns', dest='occ_hns', action="store_true",default= False, help=" Add multiple random occlusion boxes.")
parser.add_argument('-or', '--occlusion-rnd', dest='occ_rnd', action="store_true",default= False, help=" Add an occlusion box in the space of interest.")
parser.add_argument('-p', '--perspective', dest='persp', action="store_true",default= False, help=" Apply a random perspective transform to the image.")
parser.add_argument('-pz', '--posterize', dest='posterize', action="store_true",default= False, help=" Apply image posterization.")
parser.add_argument('-ra', '--rain', dest='rain', action="store_true",default= False, help=" Apply rain filter.")
parser.add_argument('-rn', '--rotnov', dest='conv_rotnov', action="store_true",default= False, help=" Rotate 90 degrees.")
parser.add_argument('-rc', '--rotcien', dest='conv_rotcien', action="store_true",default= False, help=" Rotate 180 degrees.")
parser.add_argument('-rd', '--rotdos', dest='conv_rotdos', action="store_true",default= False, help=" Rotate 270 degrees.")
parser.add_argument('-rx', '--rotx', dest='conv_rotx', action="store", type=float,default= False, help=" Rotate <X> degrees.")
parser.add_argument('-rmx', '--rot-mult', dest='mult_rot', action="store", type=int,default= False, help=" Apply multiple rotation, rotating <N> times each 360/Nº")
parser.add_argument('-s', '--resize', dest='resize', action="store_true",default= False, help=" Resize the image without changing its dimensions.")
parser.add_argument('-sh', '--sharpen', dest='sharpen', action="store_true",default= False, help=" Apply sharpen filtering.")
parser.add_argument('-so', '--solarize', dest='solarize', action="store_true",default= False, help=" Apply image solarization.")
parser.add_argument('-sm', '--smooth', dest='smooth', action="store_true",default= False, help=" Apply image smoothing.")
parser.add_argument('-tr', '--translate', dest='translate', action="store_true",default= False, help=" Apply image translation.")
parser.add_argument('-z', '--zoom', dest='zoom', action="store_true",default= False, help=" Apply zoom (scaling) and crop image.")
parser.add_argument('-zh', '--zoomh', dest='zoomh', action="store_true",default= False, help=" Apply horizontal scaling and crop image.")
parser.add_argument('-zv', '--zoomv', dest='zoomv', action="store_true",default= False, help=" Apply vertical scaling and crop image.")

options = parser.parse_args()

# Ordering methods
def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]


images = glob.glob(re.sub(r'\\', '/', os.path.abspath(options.input_folder)) + "/*.jpg")
images.sort(key=natural_keys)

dataset = Extender(images, xml_output=True, txt_output=True)

# Choose output folder. If an output folder was given
if options.output_folder:
    folder_output=options.output_folder
else:
    # If an output folder wasn't given, create the default output folder
    temppath = re.sub(r'\\', '/', os.path.abspath(images[0]))
    folder_name = temppath.split('/')[-2]
    temppath = temppath.split('/')[:-2]
    direct = '/'.join(temppath)
    folder_output = f'{direct}/output'

try:
    os.stat(folder_output)
except:
    os.mkdir(folder_output)

#**********************************
#   Boundbox Transformations
if options.bbt == 1 or options.all == 1 :
    print ("Dataset expansion: Applying boundbox transformations...")
    dataset.extend(folder_output, "BBT", "bi", [Extender.BNDBOX_BILATERAL])
    dataset.extend(folder_output, "BBT", "bw", [Extender.BNDBOX_BLACK_AND_WHITE])
    dataset.extend(folder_output, "BBT", "bl", [Extender.BNDBOX_BLUR], blur = 7)
    dataset.extend(folder_output, "BBT", "bc", [Extender.BNDBOX_BRIGHTNESS_CONTRAST])
    dataset.extend(folder_output, "BBT", "c", [Extender.BNDBOX_COLOR])
    dataset.extend(folder_output, "BBT", "di", [Extender.BNDBOX_DILATION])
    dataset.extend(folder_output, "BBT", "er", [Extender.BNDBOX_EROSION])
    dataset.extend(folder_output, "BBT", "m", [Extender.BNDBOX_MIRROR], mirror=0)
    dataset.extend(folder_output, "BBT", "mv", [Extender.BNDBOX_MIRROR], mirror=1)
    dataset.extend(folder_output, "BBT", "mh", [Extender.BNDBOX_MIRROR], mirror=2)
    dataset.extend(folder_output, "BBT", "noi", [Extender.BNDBOX_NOISE])
    dataset.extend(folder_output, "BBT", "pst", [Extender.BNDBOX_POSTERIZE], posterize=2)
    dataset.extend(folder_output, "BBT", "sm", [Extender.BNDBOX_SMOOTH])
    dataset.extend(folder_output, "BBT", "so", [Extender.BNDBOX_SOLARIZE])
    dataset.extend(folder_output, "BBT", "rx", [Extender.BNDBOX_ROTATE])
    dataset.extend(folder_output, "BBT", "tx", [Extender.BNDBOX_TRANSLATE])
    

#**********************************
#   Brightness and contrast
if options.acbrillo == 1 or options.all == 1 :
    print ("Dataset expansion: Applying brightness and contrast...")
    dataset.extend(folder_output, "BR-CT", "bc", [Extender.BRIGHTNESS_CONTRAST])

#**********************************
#   Bilateral Filter
if options.bil_filter == 1 or options.all == 1 :
    print ("Dataset expansion: Applying bilateral denoising filter...")
    dataset.extend(folder_output, "BILAT", "bi", [Extender.BILATERAL])

#**********************************
#   Blur
if options.add_blur == 1 or options.all == 1 :
    print ("Dataset expansion: Blurring...")
    dataset.extend(folder_output, "BLUR", "bl", [Extender.BLUR])

#**********************************
#   Black and white
if options.bw_full or options.all == 1:
    print ("Dataset expansion: Converting to black and white and applying all transformations...")
    temppath = re.sub(r'\\', '/', os.path.abspath(images[0]))
    folder_name = temppath.split('/')[-2]
    for transformation in list(filter(lambda name: ( (not(name.startswith("_"))) and (name.isupper()) and (not(name.lower().endswith("black_and_white"))) ), dir(dataset))):
        dataset.extend(folder_output+'/'+folder_name+'-BW-Tr', f"{transformation.upper()}", f"{transformation.lower()}", [Extender.BLACK_AND_WHITE, getattr(dataset,transformation)])
        
if options.bw == 1 or options.bw_full or options.all == 1 :
    print ("Dataset expansion: Converting to black and white...")
    dataset.extend(folder_output, "BW", "bw", [Extender.BLACK_AND_WHITE])

#**********************************
#   Cartoon
if options.cartoon == 1 or options.all == 1 :
    print ("Dataset expansion: Applying cartoon filter...")
    dataset.extend(folder_output, "CARTOON", "ctoon", [Extender.CARTOON])

#**********************************
#   Clahe
if options.clahe == 1 or options.all == 1 :
    print ("Dataset expansion: Applying clahe filter...")
    dataset.extend(folder_output, "CLAHE", "clahe", [Extender.CLAHE])

#**********************************
#   Color-map filters

if options.color_invert == 1 or options.all == 1 :
    print ("Dataset expansion: Inverting colors...")
    dataset.extend(folder_output, "C-INVERT", "ci", [Extender.COLOR_INVERT])

if options.mix_color == 1 or options.all == 1 :
    print ("Dataset expansion: Modifying color map...")
    dataset.extend(folder_output, "COLOR", "cm", [Extender.COLOR_MAP])

if options.color_filter == 1 or options.all == 1 :
    print ("Dataset expansion: Applying color filter...")
    dataset.extend(folder_output, "C-FILTER", "cf", [Extender.COLOR_FILTER])

if options.color_pseudo == 1 or options.all == 1 :
    print ("Dataset expansion: Applying pseudo color filter...")
    dataset.extend(folder_output, "C-PSEUDO", "cp", [Extender.COLOR_PSEUDO])

#**********************************
#   Cut-mix
if options.cut_mix == 1 or options.all == 1 :
    print ("Dataset expansion: Applying cut-mix...")
    dataset.extend(folder_output, "CUTMIX", "cmix", [Extender.CUT_MIX])

#**********************************
#   Dilate & Erode
if options.dilation == 1 or options.all == 1 :
    print ("Dataset expansion: Dilating images...")
    dataset.extend(folder_output, "DIL", "di", [Extender.DILATION])

if options.erosion == 1 or options.all == 1 :
    print ("Dataset expansion: Eroding images...")
    dataset.extend(folder_output, "ERO", "ero", [Extender.EROSION])

#**********************************
#   Equalize
if options.equalize == 1 or options.all == 1 :
    print ("Dataset expansion: Equalizing images...")
    dataset.extend(folder_output, "EQ", "eq", [Extender.EQUALIZE])

#**********************************
#   Edge
if options.edge == 1 or options.all == 1 :
    print ("Dataset expansion: Applying border detection...")
    dataset.extend(folder_output, "EDG", "edg", [Extender.EDGE])

#**********************************
#   Zoom and scaling
if options.zoom == 1 or options.all == 1 :
    print ("Dataset expansion: Apllying scaling")
    dataset.extend(folder_output, "CROP", "c", [Extender.SCALE], scale=0)

if options.zoomv == 1 or options.all == 1 :
    print ("Dataset expansion: Applying vertical scaling")
    dataset.extend(folder_output, "CROPV", "cv", [Extender.SCALE], scale=1)

if options.zoomh == 1 or options.all == 1 :
    print ("Dataset expansion: Applying horizontal scaling")
    dataset.extend(folder_output, "CROPH", "ch", [Extender.SCALE], scale=2)

#**********************************
#   Mirroring
if options.conv_hor == 1 or options.all == 1 : 
    print ("Dataset expansion: Applying horizontal mirroring...")
    dataset.extend(folder_output, "MIRROR-H", "mh", [Extender.MIRROR], mirror=2)

if options.conv_ver == 1 or options.all == 1 :
    print ("Dataset expansion: Applying vertical mirroring...")
    dataset.extend(folder_output, "MIRROR-V", "mv", [Extender.MIRROR], mirror=1)

if options.conv_hor_ver == 1 or options.all == 1 :
    print ("Dataset expansion: Applying mixed mirroring...")
    dataset.extend(folder_output, "MIRROR-HV", "mhv", [Extender.MIRROR], mirror=0)


#**********************************
#   Mixup
if options.mixup == 1 or options.all == 1 :
    print ("Dataset expansion: Applying images mixup...")
    dataset.extend(folder_output, "MIXUP", "mu", [Extender.MIXUP])

#**********************************
#   Mosaic
if options.mosaico == 1 or options.all == 1 :
    print ("Dataset expansion: Applying mosaic transformation...")
    dataset.extend(folder_output, "MOSAIC", "ms", [Extender.MOSAIC])

#**********************************
#   Motion Blur
if options.motion_blur == 1 or options.all == 1 :
    print ("Dataset expansion: Applying motion blur...")
    dataset.extend(folder_output, "Motion-blur", "mb", [Extender.MOTION_BLUR])

#**********************************
#   Noise
if options.agg_noise == 1 or options.all == 1 :
    print ("Dataset expansion: Adding noise...")
    dataset.extend(folder_output, "NOI", "noi", [Extender.NOISE])

#**********************************
#   Occlusion
if options.occ_rnd == 1 or options.all == 1 :
    print ("Dataset expansion: Applying random occlusion...")
    dataset.extend(folder_output, "OCC_RND", "ornd", [Extender.OCCLUSION_RANDOM])

if options.occ_hns == 1 or options.all == 1 :
    print ("Dataset expansion: Applying multiple occlusion...")
    dataset.extend(folder_output, "OCC_HNS", "ohns", [Extender.OCCLUSION_HNS])

if options.occ_msk or options.all == 1 :
    print ("Dataset expansion: Applying mask occlusion...")
    if options.occ_msk>0:
        dataset.extend(folder_output, "OCC_MSK", "omsk", [Extender.OCCLUSION_MASK], occlusion_mask=options.occ_msk)
    elif options.all==1 or options.occ_msk<=0:
        dataset.extend(folder_output, "OCC_MSK", "omsk", [Extender.OCCLUSION_MASK])

#**********************************
#   Perspective
if options.persp == 1 or options.all == 1 :
    print ("Dataset expansion: Applying random perspective...")
    dataset.extend(folder_output, "PERSP", "p", [Extender.PERSPECTIVE])

#**********************************
#   Posterize
if options.posterize == 1 or options.all == 1 :
    print ("Dataset expansion: Applying image posterization...")
    dataset.extend(folder_output, "POST", "po", [Extender.POSTERIZE])

#**********************************
#   Rotate
if options.conv_rotnov == 1 or options.all == 1 :
    print ("Dataset expansion: Rotating 90°...")
    dataset.extend(folder_output, "R-090", "90", [Extender.ROTATE], rotate=90)

if options.conv_rotcien == 1 or options.all == 1 :
    print ("Dataset expansion: Rotating 180°...")
    dataset.extend(folder_output, "R-180", "180", [Extender.ROTATE], rotate=180)

if options.conv_rotdos == 1 or options.all == 1 :
    print ("Dataset expansion: Rotating 270°...")
    dataset.extend(folder_output, "R-270", "270", [Extender.ROTATE], rotate=270)

if options.conv_rotx :
    print (f"Dataset expansion: Rotating {options.conv_rotx}º...")
    dataset.extend(folder_output, f"R-{round(options.conv_rotx,4)}", f"{round(options.conv_rotx,4)}", [Extender.ROTATE], rotate=options.conv_rotx)

if options.mult_rot:
    print (f"Dataset expansion: Rotating {options.mult_rot} times...")
    for i in range(1,options.mult_rot+1):
        if int(i*360/options.mult_rot) % 360 != 0:
            ang = i*360/options.mult_rot
            dataset.extend(folder_output, f"R-{round(ang,4)}", f"{round(ang,4)}", [Extender.ROTATE], rotate=ang)

#**********************************
#   Rain
if options.rain == 1 or options.all == 1 :
    print ("Dataset expansion: Applying rain...")
    dataset.extend(folder_output, "RAIN", "ra", [Extender.RAIN])

#**********************************
#   Resize
if options.resize == 1 or options.all == 1 :
    print ("Dataset expansion: Resizing...")
    dataset.extend(folder_output, "RESIZE", "rsz", [Extender.RESIZE])

#**********************************
#   Sharpen
if options.sharpen == 1 or options.all == 1 :
    print ("Dataset expansion: Applying sharpen...")
    dataset.extend(folder_output, "SHARP", "sh", [Extender.SHARPEN])

#**********************************
#   Smooth
if options.smooth == 1 or options.all == 1 :
    print ("Dataset expansion: Smoothing image...")
    dataset.extend(folder_output, "SMOOTH", "smth", [Extender.SMOOTH])

#**********************************
#   Solarize
if options.solarize == 1 or options.all == 1 :
    print ("Dataset expansion: Applying solarization...")
    dataset.extend(folder_output, "SOLARIZE", "sol", [Extender.SOLARIZE])

#**********************************
#   Translate
if options.translate == 1 or options.all == 1 :
    print ("Dataset expansion: Applying translation to the image...")
    dataset.extend(folder_output, "TRNSLATE", "tr", [Extender.TRANSLATE])

del dataset