import os
from argparse import ArgumentParser
from optparse import OptionParser
import glob 
import re
from extenders import Extender

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]


if __name__ == '__main__':

    parser = ArgumentParser(description='Creates a new dataset from an existing one')
    parser.add_argument( "--input", dest="input_folder", action="store", type=str, required=True, help="Input folder" )
    parser.add_argument( "--output", dest="output_folder", action="store", type=str, required=False, help="Output folder (default folder: <input>/output)" )
    options = parser.parse_args()
    start=''

    def get_dir_size(path='.'):
        def get_size(path):
            total = 0
            with os.scandir(path) as it:
                for entry in it:
                    if entry.is_file():
                        total += entry.stat().st_size
                    elif entry.is_dir():
                        total += get_dir_size(entry.path)
            return total
        
        size = get_size(path)
        raw_size = size
        types = {0:'B', 1:'KB', 2:'MB', 3:'GB'}
        size_type = 0
        while size>1024:
            size = size/1024
            size_type+=1
            if size_type==3:
                break
        
        return round(size,2), types[size_type], raw_size
    
    def get_extended_size(raw_size):
        total = 0
        for j, _ in enumerate(Extender.ALL_MODULES):
            total +=1
            total += len(Extender.ALL_MODULES)-j-1
        
        size = raw_size * total * 1.8   #Some image transformations sizes are far greater than the original.
        types = {0:'B', 1:'KB', 2:'MB', 3:'GB'}
        size_type = 0
        while size>1024:
            size = size/1024
            size_type+=1
            if size_type==3:
                break

        return round(size,2), types[size_type]


    while not(start=='Y' or start=='y' or start=='N' or start=='n'):
        print("The process will take a while and use a huge amount of space.")
        size, size_type, raw_size = get_dir_size(options.input_folder)
        extended_size, extended_size_type = get_extended_size(raw_size)
        print(f'Input size: {size} [{size_type}]. Approx output size: {extended_size} [{extended_size_type}]')
        start=input("Are you sure you want to continue? [Y/N]> ")
    

    if start=='Y' or start=='y':
        images = glob.glob(options.input_folder + "/*.jpg")
        images.sort(key=natural_keys)

        # Choose output folder. If an output folder was given
        if options.output_folder:
            output_folder=options.output_folder
        else:
            # If an output folder wasn't given, create the default output folder
            temppath = re.sub(r'\\', '/', os.path.abspath(images[0]))
            folder_name = temppath.split('/')[-2]
            temppath = temppath.split('/')[:-2]
            direct = '/'.join(temppath)
            output_folder = f'{direct}/output'

        try:
            os.stat(output_folder)
        except:
            os.mkdir(output_folder)

        toextend = Extender(images, xml_output=True, txt_output=True)
        for j, module in enumerate(Extender.ALL_MODULES):
            print(f"Extending: {module}...")
            toextend.extend(output_folder, module, module, [module])

            for i in range(j+1,len(Extender.ALL_MODULES)):
                next_module = Extender.ALL_MODULES[min(i,len(Extender.ALL_MODULES)-1)]
                if (module.startswith("bndbox_") or next_module.startswith("bndbox_")) and (module[7:] == next_module) or (next_module[7:] == module):
                    continue

                print(f"Extending: {module}-{next_module}...")
                toextend.extend(output_folder, f"{module}-{next_module}", f"{module}-{next_module}", [module, next_module])

