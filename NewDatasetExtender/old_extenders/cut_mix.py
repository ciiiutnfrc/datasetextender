import cv2
import numpy as np
import xml.etree.ElementTree as ET 
from extenders.save_files import create_folders, File_Manager
import os
import re
import random
from random import randint

def image_cut_mix(cut_img, orig_img):
    '''
        This method recieves an image and returns the image
        with a random perspective resize warp
        The output image size and the shape of the data will
        be the same, but the image content will be in a smaller region
    '''
    or_height,or_width = orig_img.shape[:2]
    cut_height, cut_width = cut_img.shape[:2]
    in_pts = np.float32([[0,0], [cut_width-1,0], [cut_width-1,cut_height-1], [0,cut_height-1]])

    if or_height*or_width<500:
        small_limit=cut_width*cut_height/10
    else:
        small_limit=150
    
    while(True):
        x_left = randint(0, or_width-cut_width)
        x_right = randint(max(0,x_left-cut_width), x_left+cut_width)
        y_up = randint(0, or_height-cut_height)
        y_down = randint(max(0,y_up-cut_height), y_up+cut_height)

        too_big = (abs(x_right-x_left)*abs(y_down-y_up)) > or_height*or_width/10
        too_small = (abs(x_right-x_left)*abs(y_down-y_up)) < small_limit
        if not (too_big or too_small):
            break

    out_pts = np.float32([[x_left,y_up],
                        [x_right,y_up],
                        [x_right,y_down],
                        [x_left,y_down]])
    M=cv2.getPerspectiveTransform(in_pts,out_pts)
    warped_image = cv2.warpPerspective(cut_img, M, (or_width, or_height))
    
    mask = np.zeros([or_height,or_width], dtype=np.uint8)
    cv2.fillConvexPoly(mask, np.int32([out_pts]), (255, 255, 255), cv2.LINE_AA)
        
    orig_img[np.where(mask == 255)] = warped_image[np.where(mask == 255)]
    return orig_img, M

def fix_points(pts, rotation_mat):
    """
        Given a transformation matrix, fixes the points to the points where they should be
    """

    xmin, xmax, ymin, ymax = pts[0], pts[1], pts[2], pts[3]
    tr_pts = np.float32([[xmin,ymin],[xmax,ymin],[xmin,ymax],[xmax,ymax]]).reshape(-1,1,2)

    rotated_points = cv2.perspectiveTransform(tr_pts, rotation_mat)

    out_min_x=rotated_points[0][0][0]
    out_max_x=-1
    out_min_y=rotated_points[0][0][1]
    out_max_y=-1

    for point in rotated_points:
        if point[0][0]>out_max_x:
            out_max_x=point[0][0]
        if point[0][0]<out_min_x:
            out_min_x=point[0][0]

        if point[0][1]>out_max_y:
            out_max_y=point[0][1]
        if point[0][1]<out_min_y:
            out_min_y=point[0][1]

    return int(out_min_x), int(out_max_x), int(out_min_y), int(out_max_y)

def cut_mix(images, output_folder, folder_tag='CUTMIX', file_tag='CM') -> None:

    create_folders(images[0], output_folder, folder_tag)

    for file_name in images:
        f_name = re.sub(r'\\', '/', os.path.abspath(file_name))

        img = cv2.imread(f_name)

        # Read label files and create a root where the objects can be read
        files = File_Manager(file_name)
        root = files.root
        if root is None:
            print(f'The file: "{f_name}" has no labels.\n'+
                    "Please label the image and give it the same name as the image's")
            del files
            continue

        size_xml = root.find('size')
        width_xml = int(size_xml.find('width').text)
        height_xml = int(size_xml.find('height').text)
        
        out_img = img.copy()

        # Modifico xml                         
        for object_xml in root.findall('object'):

            bndbox_xml = object_xml.find('bndbox')
            xmin_xml = int(bndbox_xml.find('xmin').text)
            xmax_xml = int(bndbox_xml.find('xmax').text)
            ymin_xml = int(bndbox_xml.find('ymin').text)
            ymax_xml = int(bndbox_xml.find('ymax').text)

            for _ in range(randint(1,3)):
                out_img, mtx = image_cut_mix(img[ymin_xml:ymax_xml,xmin_xml:xmax_xml,:], out_img)
                
                n_xmin, n_xmax, n_ymin, n_ymax = fix_points([0,xmax_xml-xmin_xml,0,ymax_xml-ymin_xml], mtx)

                obj = ET.SubElement(root, 'object')
                ET.SubElement(obj, 'name').text = object_xml.find('name').text
                ET.SubElement(obj, 'pose').text = 'Unspecified'
                ET.SubElement(obj, 'truncated').text = '0'
                ET.SubElement(obj, 'difficut').text = '0'
                bndbox = ET.SubElement(obj, 'bndbox')
                ET.SubElement(bndbox, 'xmin').text = str(n_xmin)
                ET.SubElement(bndbox, 'ymin').text = str(n_ymin)
                ET.SubElement(bndbox, 'xmax').text = str(n_xmax)
                ET.SubElement(bndbox, 'ymax').text = str(n_ymax)
        
        File_Manager.indent(files, root)

        width_aux = size_xml.find('width')
        height_aux = size_xml.find('height')
        
        width_aux.text = str(width_xml)
        height_aux.text = str(height_xml)

        # Save labels and images, also delete object in memory
        folder_name=f_name.split('/')[-2] + f'-{folder_tag}'
        files.save_files(output_folder, folder_name, file_tag)
        cv2.imwrite(output_folder + '/' + f_name.split('/')[-2] + f'-{folder_tag}/' + f'{file_tag}_' + f_name.split('/')[-1], out_img)
        del files