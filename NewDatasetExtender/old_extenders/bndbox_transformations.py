import cv2
import numpy as np
import xml.etree.ElementTree as ET
from extenders.save_files import create_folders, File_Manager
import os
import re
import random
import math as ma

def rotate_image(mat, angle):
    """
        Rotates an image (angle in degrees) and expands image to avoid cropping
    """

    height, width = mat.shape[:2] # image shape has 3 dimensions
    image_center = (width/2, height/2) # getRotationMatrix2D needs coordinates in reverse order (width, height) compared to shape

    rotation_mat = cv2.getRotationMatrix2D(image_center, angle, 1.)

    # rotation calculates the cos and sin, taking absolutes of those.
    abs_cos = abs(rotation_mat[0,0]) 
    abs_sin = abs(rotation_mat[0,1])

    # find the new width and height bounds
    bound_w = int(height * abs_sin + width * abs_cos)
    bound_h = int(height * abs_cos + width * abs_sin)

    # subtract old image center (bringing image back to origo) and adding the new image center coordinates
    rotation_mat[0, 2] += bound_w/2 - image_center[0]
    rotation_mat[1, 2] += bound_h/2 - image_center[1]

    # rotate image with the new bounds and translated rotation matrix
    rotated_mat = cv2.warpAffine(mat, rotation_mat, (bound_w, bound_h))
    return rotated_mat, bound_w, bound_h

def img_solarize(im):
        solarization_const = random.uniform(1,3) * np.pi / 255
        look_up_table = np.ones((256, 1), dtype = 'uint8' ) * 0

        for i in range(256):
            look_up_table[i][0] = np.abs(np.sin(i * solarization_const)) * 100

        img_sola = cv2.LUT(im, look_up_table)

        return img_sola

def im_posterize(im, level):
        n = 2**level  # Number of levels of quantization
        indices = np.arange(0,256)   # List of all colors 
        divider = np.linspace(0,255,n+1)[1] # we get a divider
        quantiz = np.int0(np.linspace(0,255,n)) # we get quantization colors
        color_levels = np.clip(np.int0(indices/divider),0,n-1) # color levels 0,1,2..
        palette = quantiz[color_levels] # Creating the palette
        im2 = palette[im]  # Applying palette on image
        im2 = cv2.convertScaleAbs(im2) # Converting image back to uint8
        return im2

def sp_noise(image):
        row,col,ch= image.shape
        mean = 0
        var = 4
        sigma = var**3
        gauss = np.random.normal(mean,sigma,(row,col,ch))
        gauss = gauss.reshape(row,col,ch)
        noisy = image + gauss
        return noisy

def im_erosion(image,level):
        neighbourhood8 = np.array([[1, 1, 1],
                                [1, 1, 1],
                                [1, 1, 1]],
                                np.uint8)
        img_erosion = cv2.erode(image,
                                neighbourhood8,
                                iterations=level)
        return img_erosion

def im_dilation(image, level):
        neighbourhood8 = np.array([[1, 1, 1],
                                [1, 1, 1],
                                [1, 1, 1]],
                                np.uint8)
        img_dilation = cv2.dilate(image,
                                neighbourhood8,
                                iterations=level)
        return img_dilation

def modify_bndbox(bndbox_img, shape, orig_pts, mod_type):
    or_height, or_width, or_ch = shape
    bndbox_height, bndbox_width = bndbox_img.shape[:2]

    if mod_type!=2 or (mod_type in range(7,10)):
        in_pts = np.float32([[0,0], [bndbox_width-1,0], [bndbox_width-1,bndbox_height-1]])
        out_pts = np.float32(orig_pts)
        M = cv2.getAffineTransform(in_pts, out_pts)
        wrk_img = cv2.warpAffine(bndbox_img, M, (or_width,or_height))

    #trf_mat=np.float32([[s*ma.cos(ang),-s*ma.sin(ang)],[s*ma.sin(ang),s*ma.cos(ang)]])
    if mod_type==1:
        '''Desplazamiento aleatorio'''
        
        x_despl=random.randint(-bndbox_width//2, bndbox_width//2)
        y_despl=random.randint(-bndbox_height//2, bndbox_height//2)

        M=np.float32([[1,0,x_despl],[0,1,y_despl]])

        #in_pts = np.float32([[0,0], [bndbox_width-1,0], [bndbox_width-1,bndbox_height-1]])
        #out_pts = np.float32([[x_despl,y_despl], [bndbox_width-1+x_despl,y_despl], [bndbox_width-1+x_despl,bndbox_height-1+y_despl]])
        #M = cv2.getAffineTransform(in_pts, out_pts)

        type1_img = cv2.warpAffine(wrk_img, M, (or_width,or_height))
        return type1_img
    
    elif mod_type==2:
        '''Rotacion aleatoria'''
        
        ang=random.randint(1,359)
        trimg, bound_w, bound_h = rotate_image(bndbox_img,ang)

        in_pts = np.float32([[0,0], 
                            [bndbox_width-1,0], 
                            [bndbox_width-1,bndbox_height-1]])
        out_pts = np.float32(orig_pts)
        M = cv2.getAffineTransform(in_pts, out_pts)
        type2_img = cv2.warpAffine(trimg, M, (or_width,or_height))
        
        return type2_img

    elif mod_type==3:
        return img_solarize(wrk_img)
    
    elif mod_type==4:
        return cv2.medianBlur(wrk_img,random.randrange(5, 9+1, 2))

    elif mod_type==5:
        return im_posterize(wrk_img,random.randint(1,3))

    elif mod_type==6:
        return sp_noise(wrk_img)
    
    elif mod_type>=7 and mod_type<=9:
        bndbox_img = cv2.flip(bndbox_img, mod_type-8)
        in_pts = np.float32([[0,0], [bndbox_width-1,0], [bndbox_width-1,bndbox_height-1]])
        out_pts = np.float32(orig_pts)
        M = cv2.getAffineTransform(in_pts, out_pts)
        return cv2.warpAffine(bndbox_img, M, (or_width,or_height))

    elif mod_type==10:
        return im_erosion(wrk_img, random.randint(1,4))
    elif mod_type==11:
        return im_dilation(wrk_img, random.randint(1,4))
    elif mod_type==12:
        return cv2.applyColorMap(wrk_img, random.randint(0,11))
    elif mod_type==13:
        return cv2.convertScaleAbs(wrk_img, alpha=random.uniform(1.0,3.0), beta=random.randrange(20,80))
    elif mod_type==14:
        return cv2.blur(wrk_img, (random.randint(5,15),random.randint(5,15)))
    elif mod_type==15:
        img_gry = cv2.cvtColor(wrk_img, cv2.COLOR_BGR2GRAY)
        return cv2.merge((img_gry,img_gry,img_gry)) 
    elif mod_type==16:
        return cv2.bilateralFilter(wrk_img, 25, 75, 75)

def boundbox_transformations(images, output_folder, folder_tag='BndBoxTr', file_tag='BBT') -> None:

    create_folders(images[0], output_folder, folder_tag)
    
    for (key,ftag) in ftag_keys.items():
        file_tag='BBT' + ftag
        for file_name in images:
            f_name = re.sub(r'\\', '/', os.path.abspath(file_name))

            img = cv2.imread(f_name)

            # Read label files and create a root where the objects can be read
            files = File_Manager(file_name)
            root = files.root
            if root is None:
                print(f'The file: "{f_name}" has no labels.\n'+
                        "Please label the image and give it the same name as the image's")
                del files
                continue

            size_xml = root.find('size')
            width_xml = int(size_xml.find('width').text)
            height_xml = int(size_xml.find('height').text)

            # Modifico xml                         
            for object_xml in root.findall('object'):
                
                bndbox_xml = object_xml.find('bndbox')
                xmin_xml = int(bndbox_xml.find('xmin').text)
                xmax_xml = int(bndbox_xml.find('xmax').text)
                ymin_xml = int(bndbox_xml.find('ymin').text)
                ymax_xml = int(bndbox_xml.find('ymax').text)
                ymin_aux = bndbox_xml.find('ymin')
                ymax_aux = bndbox_xml.find('ymax')
                xmin_aux = bndbox_xml.find('xmin')
                xmax_aux = bndbox_xml.find('xmax')
                
                mask = np.zeros([height_xml,width_xml], dtype=np.uint8)
                cv2.fillConvexPoly(mask, np.int32([[xmin_xml,ymin_xml],[xmax_xml,ymin_xml],[xmax_xml,ymax_xml],[xmin_xml,ymax_xml]]), (255, 255, 255), cv2.LINE_AA)
                
                img_modif = modify_bndbox(img[ymin_xml:ymax_xml,xmin_xml:xmax_xml,:], img.shape, 
                                        [[xmin_xml,ymin_xml],[xmax_xml,ymin_xml],[xmax_xml,ymax_xml]], mod_type=key)

                img[np.where(mask==255)] = img_modif[np.where(mask==255)]

            width_aux = size_xml.find('width')
            height_aux = size_xml.find('height')
            
            width_aux.text = str(width_xml)
            height_aux.text = str(height_xml)

            # Save labels and images, also delete object in memory
            folder_name=f_name.split('/')[-2] + f'-{folder_tag}'
            files.save_files(output_folder, folder_name, file_tag)
            cv2.imwrite(output_folder + '/' + f_name.split('/')[-2] + f'-{folder_tag}/' + f'{file_tag}_' + f_name.split('/')[-1], img)
            del files