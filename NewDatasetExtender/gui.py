#! /usr/bin/env python 
# -*- coding: utf-8 -*- 
# vim:fenc=utf-8 
#
# Copyright © 2022 Ciii UTN-FRC  <agus.req@gmail.com> 

import tkinter as tk
import customtkinter as ctk
from tkinter import filedialog
from extenders.extender_app import Extender
import os
import re
import glob
import random

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

class App(ctk.CTk):
    '''
    App Class
    '''
    def __init__(self):
        super().__init__()

        self.configuration()
        self.set_variables()
        self.main_window()

    def configuration(self):
        '''
        Tkinter and CTkinter configurations
        '''
        ctk.set_appearance_mode("dark")  # Modes: system (default), light, dark
        ctk.set_default_color_theme("blue")  # Themes: blue (default), dark-blue, green
        self.geometry("535x375")
        self.title("Dataset Extender GUI")
        self.resizable(False,False)

    def set_variables(self):
        '''
        Variable Definitions
        '''
        self.in_folder = ctk.StringVar(self,'')
        self.out_folder = ctk.StringVar(self,'')
        self.action = ctk.StringVar(self,'© 2022 Ciii UTN-FRC')
        self.modules={}

        temppath = re.sub(r'\\', '/', os.path.abspath(__file__))
        temppath = '/'.join(temppath.split('/')[:-1])
        for module in os.listdir(os.path.dirname(temppath+'/extenders/img_transformations/')):
            if module == '__init__.py' or module[-3:] != '.py' or module[:2] == '__':
                continue
            self.modules[module[:-3]]=module[:-3]
        
        temppath = re.sub(r'\\', '/', os.path.abspath(__file__))
        temppath = '/'.join(temppath.split('/')[:-1])
        for module in os.listdir(os.path.dirname(temppath+'/extenders/bndbox_transformations/')):
            if module == '__init__.py' or module[-3:] != '.py' or module[:2] == '__':
                continue
            self.modules[module[:-3]]=module[:-3]

    def main_window(self):
        '''
        Main window creation.
        '''
        self.input_bar(self, text="Input Directory", foldervar=self.in_folder, bar_pos=(10, 10), button_pos=(410, 10))

        optionsframe = self.options_selection(self)
        self.options_frame(self, (10,50), (260-15, 235), self.extenders_selection, optionsframe.update_options)
        self.options_frame(self, (306-15,50), (192+15, 236), optionsframe.create, None)
        load, action_status = self.load_bar(self, pos=(100,340))
        self.input_bar(self, text="Output Directory", foldervar=self.out_folder, bar_pos=(10, 300), button_pos=(410, 300))
        self.start_button(self, (10,340), load, action_status, optionsframe)
    
    def input_bar(self, master, text, foldervar, bar_pos:tuple, button_pos:tuple):
        '''
        Input bar creation.
        Here we create an entry bar and a button for directory input.
        '''
        def dir_select(foldervar, entry):
            folder = filedialog.askdirectory(initialdir='./', title="Select an input folder")
            if folder and folder is not None:
                foldervar.set(folder)
            entry.entry.xview(ctk.END)
        
        (bar_x, bar_y) = bar_pos
        (button_x, button_y) = button_pos
        dir_entry = ctk.CTkEntry(master, textvariable=foldervar, corner_radius=16, width=390) 
        dir_entry.place(x=bar_x, y=bar_y)
        dir_b = ctk.CTkButton(master, text=text, width=115, corner_radius=8, command=lambda: dir_select(foldervar, dir_entry)) 
        dir_b.place(x=button_x, y=button_y)
    
    class options_frame():
        '''
        Options frame creation.
        Here we create a frame in which we will introduce its CONTENTS and possibly link an EVENT
        '''
        def __init__(self, master, pos:tuple, size:tuple, contents, event_conection):
            (pos_x, pos_y) = pos
            (size_width, size_height) = size

            frame = ctk.CTkFrame(master, fg_color='#EEEEEE', width=size_width, height=size_height, corner_radius=5)
            
            canvas = ctk.CTkCanvas(frame, width=size_width, height=size_height, bg="#EEEEEE", highlightthickness=0) #bg='#393E46'
            canvas.pack(side=tk.LEFT, padx=(5,0), pady=1, expand=1) 
            scroll = tk.Scrollbar(frame, orient=tk.VERTICAL, command=canvas.yview, highlightthickness=0) 
            scroll.pack(side=tk.RIGHT, fill='y', padx=(0,5), pady=1) 

            checkboxes_frame = ctk.CTkFrame(canvas, fg_color='#EEEEEE', width=size_width, height=size_height, corner_radius=0)
            checkboxes_frame.canvas.bind('<MouseWheel>',lambda e: master.onMouseWheel(e, canvas))
            canvas.create_window((0,0), window=checkboxes_frame, anchor='nw')

            contents(checkboxes_frame, canvas, event_conection, scroll)

            canvas.configure(yscrollcommand=scroll.set)
            canvas.bind('<Configure>', lambda e: canvas.configure(scrollregion = canvas.bbox("all")))
            canvas.bind('<MouseWheel>', lambda e: master.onMouseWheel(e, canvas))
            frame.canvas.bind('<MouseWheel>', lambda e: master.onMouseWheel(e, canvas))
            frame.place(x=pos_x, y=pos_y)
    
    def extenders_selection(self, frame, canvas, event_conection, *args):
        '''
        Extenders selection. CONTENT of options_frame.
        In here we add all the checkboxes used to select and apply each type of extender.
        '''
        self.check_vars={}
        checkboxes=[]

        def update_extenders(v):
            if v.get():
                for key,var in self.check_vars.items():
                    var.set(True)
                for box in checkboxes:
                    box.select()
                    box.update()
            else:
                for key,var in self.check_vars.items():
                    var.set(False)
                for box in checkboxes:
                    box.deselect()
                    box.update()
        
        all_v = ctk.BooleanVar(frame, False)
        c = ctk.CTkCheckBox(master=frame, text=f"Apply all", variable=all_v, onvalue=True, offvalue=False, text_color='#000000', command=lambda: self.after(10, lambda e: update_extenders(all_v), self))
        c.grid(row=0, column=0, padx=(10,0), pady=(5,5), sticky=ctk.W)
        c.text_label.bind('<MouseWheel>', lambda e: self.onMouseWheel(e, canvas))
        c.canvas.bind('<MouseWheel>', lambda e: self.onMouseWheel(e, canvas))
        c.bg_canvas.bind('<MouseWheel>', lambda e: self.onMouseWheel(e, canvas))

        for i,(key,val) in enumerate(sorted(self.modules.items())):
            v = ctk.BooleanVar(frame, False)
            self.check_vars[key]=v
            c = ctk.CTkCheckBox(master=frame, text=f"Apply {key}", variable=self.check_vars[key], onvalue=True, offvalue=False, text_color='#000000', command=lambda: self.after(10, lambda e: event_conection(), self))
            checkboxes.append(c)
            c.grid(row=i+1, column=0, padx=(10,0), pady=(5,5), sticky=ctk.W)
            c.text_label.bind('<MouseWheel>', lambda e: self.onMouseWheel(e, canvas))
            c.canvas.bind('<MouseWheel>', lambda e: self.onMouseWheel(e, canvas))
            c.bg_canvas.bind('<MouseWheel>', lambda e: self.onMouseWheel(e, canvas))
    
    class options_selection():
        '''
        Options selection. CONTENT of options_frame.
        In here we add all the options related to each extender.
        Its instanciated as an inner class to make it easier to update its contents.
        '''
        def __init__(self, super):
            self.super=super
            self.horizontal_mirror = ctk.BooleanVar(super, False)
            self.vertical_mirror= ctk.BooleanVar(super, False)
            self.both_mirror= ctk.BooleanVar(super, False)
            self.rotate_times = ctk.IntVar(super,0)
            self.rotate_angle = ctk.DoubleVar(super,0)
            self.mask_size = ctk.DoubleVar(super,0)
            self.horizontal_scale = ctk.BooleanVar(super, False)
            self.vertical_scale = ctk.BooleanVar(super, False)
            self.both_scale = ctk.BooleanVar(super, False)
            self.bw_all = ctk.BooleanVar(super, False)

        def create(self, frame, canvas, event_conection, *args):
            self.frame = frame
            self.canvas = canvas
            self.scroll = args[0]

            widgets=[]

            self.bw_option = ctk.CTkLabel(master=frame, text="Black and white, apply all tr?", text_color="#000000", corner_radius=5)
            self.bw_checkmark = ctk.CTkCheckBox(master=frame, text=f"Apply all transformations", variable=self.bw_all, onvalue=True, offvalue=False, text_color='#000000')
            widgets.extend([self.bw_option, self.bw_checkmark])

            self.mirror_option = ctk.CTkLabel(master=frame, text="Select mirror type        ", text_color="#000000", corner_radius=5)
            self.mirror_checkmark_hor = ctk.CTkCheckBox(master=frame, text=f"Horizontal", variable=self.horizontal_mirror, onvalue=True, offvalue=False, text_color='#000000')
            self.mirror_checkmark_ver = ctk.CTkCheckBox(master=frame, text=f"Vertical", variable=self.vertical_mirror, onvalue=True, offvalue=False, text_color='#000000')
            self.mirror_checkmark_both = ctk.CTkCheckBox(master=frame, text=f"Both", variable=self.both_mirror, onvalue=True, offvalue=False, text_color='#000000')
            widgets.extend([self.mirror_option, self.mirror_checkmark_hor, self.mirror_checkmark_ver, self.both_mirror])

            self.rotate_option = ctk.CTkLabel(master=frame, text="Enter angle of rotation:", text_color="#000000", corner_radius=5)
            self.rotate_entry = ctk.CTkEntry(master=frame, textvariable=self.rotate_angle, corner_radius=0, width=50, justify='center', border_width=1, height=18, text_font=('',9)) 
            self.mult_rotate_option = ctk.CTkLabel(master=frame, text="How many times to multi-rotate:", text_color="#000000", corner_radius=5)
            self.mult_rotate_entry = ctk.CTkEntry(master=frame, textvariable=self.rotate_times, corner_radius=0, width=50, justify='center', border_width=1, height=18, text_font=('',9)) 
            widgets.extend([self.rotate_option, self.rotate_entry, self.mult_rotate_option, self.mult_rotate_entry])

            self.occ_mask_option = ctk.CTkLabel(master=frame, text="Enter mask box-size (0=default):", text_color="#000000", corner_radius=5)
            self.occ_mask_entry = ctk.CTkEntry(master=frame, textvariable=self.mask_size, corner_radius=0, width=50, justify='center', border_width=1, height=18, text_font=('',9)) 
            widgets.extend([self.occ_mask_entry, self.occ_mask_option])

            self.scale_option = ctk.CTkLabel(master=frame, text="Select scaling type    ", text_color="#000000", corner_radius=5)
            self.scale_checkmark_hor = ctk.CTkCheckBox(master=frame, text=f"Horizontal", variable=self.horizontal_scale, onvalue=True, offvalue=False, text_color='#000000')
            self.scale_checkmark_ver = ctk.CTkCheckBox(master=frame, text=f"Vertical", variable=self.vertical_scale, onvalue=True, offvalue=False, text_color='#000000')
            self.scale_checkmark_both = ctk.CTkCheckBox(master=frame, text=f"Both", variable=self.both_scale, onvalue=True, offvalue=False, text_color='#000000')
            widgets.extend([self.scale_option, self.scale_checkmark_hor, self.scale_checkmark_ver, self.scale_checkmark_both])

            self.link_scrollwheel(canvas, widgets)

        def link_scrollwheel(self, canvas, *args):
            for widget in args[0]:
                if type(widget) == type(ctk.CTkLabel()):
                    widget.text_label.bind('<MouseWheel>', lambda e: self.super.onMouseWheel(e, canvas))
                    widget.canvas.bind('<MouseWheel>', lambda e: self.super.onMouseWheel(e, canvas))
                elif type(widget) == type(ctk.CTkCheckBox()):
                    widget.text_label.bind('<MouseWheel>', lambda e: self.super.onMouseWheel(e, canvas))
                    widget.canvas.bind('<MouseWheel>', lambda e: self.super.onMouseWheel(e, canvas))
                    widget.bg_canvas.bind('<MouseWheel>', lambda e: self.super.onMouseWheel(e, canvas))
                elif type(widget) == type(ctk.CTkEntry()):
                    widget.canvas.bind('<MouseWheel>', lambda e: self.super.onMouseWheel(e, canvas))
                    widget.entry.bind('<MouseWheel>', lambda e: self.super.onMouseWheel(e, canvas))

        def update_options(self):
            if self.super.check_vars['black_and_white'].get():
                self.bw_option.grid(row=0, column=0, sticky=ctk.W, padx=(5,0), pady=(5,0))
                self.bw_checkmark.grid(row=1, column=0, sticky=ctk.W, padx=(35,0), pady=(0,0))
            else:
                self.bw_option.grid_forget()
                self.bw_checkmark.grid_forget()

            if self.super.check_vars['mirror'].get():
                self.mirror_option.grid(row=2, column=0, sticky=ctk.W, padx=(5,0), pady=(5,0))
                self.mirror_checkmark_hor.grid(row=3, column=0, sticky=ctk.W, padx=(35,0), pady=(0,0))
                self.mirror_checkmark_ver.grid(row=4, column=0, sticky=ctk.W, padx=(35,0), pady=(3,0))
                self.mirror_checkmark_both.grid(row=5, column=0, sticky=ctk.W, padx=(35,0), pady=(3,3))
            else:
                self.mirror_option.grid_forget()
                self.mirror_checkmark_hor.grid_forget()
                self.mirror_checkmark_ver.grid_forget()
                self.mirror_checkmark_both.grid_forget()

            if self.super.check_vars['rotate'].get():
                self.rotate_option.grid(row=6, column=0, sticky=ctk.W, padx=(5,0), pady=(3,0))
                self.rotate_entry.grid(row=7, column=0, sticky=ctk.W, padx=(36,0), pady=(0,0))
                self.mult_rotate_option.grid(row=8, column=0, sticky=ctk.W, padx=(5,0), pady=(3,0))
                self.mult_rotate_entry.grid(row=9, column=0, sticky=ctk.W, padx=(36,0), pady=(0,3))
            else:
                self.rotate_option.grid_forget()
                self.rotate_entry.grid_forget()
                self.mult_rotate_option.grid_forget()
                self.mult_rotate_entry.grid_forget()

            if self.super.check_vars['occlusion_mask'].get():
                self.occ_mask_option.grid(row=10, column=0, sticky=ctk.W, padx=(5,0), pady=(3,0))
                self.occ_mask_entry.grid(row=11, column=0, sticky=ctk.W, padx=(36,0), pady=(0,3))
            else:
                self.occ_mask_option.grid_forget()
                self.occ_mask_entry.grid_forget()
            if self.super.check_vars['scale'].get():
                self.scale_option.grid(row=12, column=0, sticky=ctk.W, padx=(5,0), pady=(5,0))
                self.scale_checkmark_hor.grid(row=13, column=0, sticky=ctk.W, padx=(35,0), pady=(0,0))
                self.scale_checkmark_ver.grid(row=14, column=0, sticky=ctk.W, padx=(35,0), pady=(3,0))
                self.scale_checkmark_both.grid(row=15, column=0, sticky=ctk.W, padx=(35,0), pady=(3,3))
            else:
                self.scale_option.grid_forget()
                self.scale_checkmark_hor.grid_forget()
                self.scale_checkmark_ver.grid_forget()
                self.scale_checkmark_both.grid_forget()
            self.frame.bind('<Configure>', lambda e: self.canvas.configure(scrollregion = self.canvas.bbox("all")))


    def start_button(self, master, pos:tuple, *args):
        '''
        Start button.
        A button that will execute the extenders code in the "start_processing()" method
        '''
        (pos_x, pos_y) = pos
        start_b = ctk.CTkButton(master, text="Start", width=80, corner_radius=8, fg_color="green", hover_color='#006500') 
    
        new_args = [start_b]
        new_args.extend(args)    
        start_b.configure(command=lambda: self.start_processing( new_args ))
        start_b.place(x=pos_x, y=pos_y)

    def load_bar(self, master, pos:tuple):
        '''
        A load bar with a text entry below to show the process status.
        '''
        (pos_x, pos_y) = pos
        load = ctk.CTkProgressBar(master, width=425, progress_color='green')
        load.configure(progress_color='#4a4d50')
        load.set(0)
        load.place(x=pos_x, y=pos_y) #y=350
        action_status = ctk.CTkEntry(master, textvariable=self.action, corner_radius=4, width=425, justify='center', border_width=1, height=17, text_font=('',7), state=ctk.DISABLED) 
        action_status.place(x=pos_x, y=pos_y+11)

        return load, action_status

    def event_pos(self, event):
        '''
        Debugging method
        The idea is to use an app bind of <Button-1> so we can get the widget that the mouse is on and it's coordinates.
        '''
        x,y = self.winfo_pointerxy()
        widget = self.winfo_containing(x,y)
        print("widget:", widget)

    def onMouseWheel(self, event, canvas):
        '''
        Mouse wheel event method.
        '''
        #self.event_pos(event)    
        canvas.yview("scroll",int(-event.delta/(len(self.modules)*2)),"units")
        return "break" 

    def start_processing(self, args):
        '''
        Method that expands the dataset based on the previous inputs.
        '''
        start_b, load, action_status, options_data = args

        #Check if input folder exists
        try:
            os.stat(self.in_folder.get())
        except OSError:
            self.action.set("ERROR: Invalid input folder.")
            return
        except:
            self.action.set("ERROR: in_folder.")
            return

        #Get images and check if there are images on the folder
        images = glob.glob(self.in_folder.get() + "/*.jpg")
        images.sort(key=natural_keys)

        if len(images)<1:
            self.action.set("ERROR: No images found on the folder.")
            return

        temppath = re.sub(r'\\', '/', os.path.abspath(images[0]))
        folder_name = temppath.split('/')[-2]

        #Check if output folder exists or if it can be created.
        try:
            os.stat(self.out_folder.get())
        except OSError:
            try:
                os.mkdir(self.out_folder.get())
            except:
                self.action.set("ERROR: Invalid output folder.")
                return
        except:
            self.action.set("ERROR: out_folder.")
            return
        
        #Get absolute path of output folder
        out_directory= re.sub(r'\\', '/', os.path.abspath(self.out_folder.get()))

        #Check if extender options are correct 
        try:
            mask_size = options_data.mask_size.get()
        except:
            mask_size = None
        try:
            rotate_angle = options_data.rotate_angle.get()
        except:
            print("Value entered was not a valid number, applying random rotation...")
            rotate_angle = random.randint(1,359)
            options_data.rotate_angle.set(rotate_angle)
        try:
            options_data.rotate_times.get()
        except:
            options_data.rotate_times.set(0)

        #Start the process
        start_b.configure(state=ctk.DISABLED)
        start_b.update()
        load.configure(progress_color='green')

        dataset = Extender(images, True, True)
        for i,(key,var) in enumerate(self.check_vars.items()):
            
            if key=='all':
                continue
            self.action.set(f'Applying {key}')
            action_status.update()

            #If the extender checkbox was selected, execute the required functionality.
            if var.get():
                if key=='black_and_white':
                    for transformation in list(filter(lambda name: ( (not(name.startswith("_"))) and (name.isupper()) and (not(name.lower().endswith("black_and_white"))) ), dir(dataset))):
                        dataset.extend(out_directory+'/'+folder_name+'-BW-Tr', f"{transformation.upper()}", f"{transformation.lower()}", [Extender.BLACK_AND_WHITE, getattr(dataset,transformation)])
                elif key=='mirror':
                    if options_data.horizontal_mirror.get():
                        dataset.extend(out_directory, "MIRROR-H", "mh", [Extender.MIRROR], mirror=2)
                    if options_data.vertical_mirror.get():
                        dataset.extend(out_directory, "MIRROR-V", "mv", [Extender.MIRROR], mirror=1)
                    if options_data.both_mirror.get():
                        dataset.extend(out_directory, "MIRROR-HV", "mhv", [Extender.MIRROR], mirror=0)
                elif key=='rotate':
                    dataset.extend(out_directory, f"R-{round(rotate_angle,4)}", f"{round(rotate_angle,4)}", [Extender.ROTATE], rotate=rotate_angle)

                    for j in range(1,options_data.rotate_times.get()+1):
                        if int(j*360/options_data.rotate_times.get()) % 360 != 0:
                            ang = j*360/options_data.rotate_times.get()
                            dataset.extend(out_directory, f"R-{round(ang,4)}", f"{round(ang,4)}", [Extender.ROTATE], rotate=ang)

                elif key=='occlusion_mask':
                    dataset.extend(out_directory, "OCC_MSK", "omsk", [Extender.OCCLUSION_MASK], occlusion_mask=mask_size)
                elif key=='scale':
                    if options_data.horizontal_scale.get():
                        dataset.extend(out_directory, "CROPH", "ch", [Extender.SCALE], scale=2)
                    if options_data.vertical_scale.get():
                        dataset.extend(out_directory, "CROPV", "cv", [Extender.SCALE], scale=1)
                    if options_data.both_scale.get():
                        dataset.extend(out_directory, "CROP", "c", [Extender.SCALE], scale=0)
                else:
                    dataset.extend(out_directory, f"{key.upper()}", f"{key.lower()}", [key])
            load.set(i/(len(self.check_vars)-1))
            load.update()
        self.action.set(f'DONE!')

        del dataset

        start_b.configure(state=ctk.ACTIVE)
        start_b.update()

        #load.configure(progress_color='#4a4d50')



if __name__ == '__main__':
    app = App()
    app.mainloop()