import cv2
import numpy as np
from .__bndbox_manager import fix_boundbox

mean = 0
var = 4
sigma = var**3

def bndbox_noise(image, img_boundbox, var, bndbox, *args):
    [xmin, ymin, xmax, ymax] = bndbox

    row,col,ch= img_boundbox.shape
    gauss = np.random.normal(mean,sigma,(row,col,ch))
    gauss = gauss.reshape(row,col,ch)
    noisy_img = img_boundbox + gauss
    noisy_img[noisy_img > 255] = 255
    noisy_img[noisy_img < 0] = 0

    b,g,r = cv2.split(noisy_img)
    img_boundbox = cv2.merge((b,g,r))
    img_boundbox = np.float32(img_boundbox)

    img_boundbox = fix_boundbox(img_boundbox, img_boundbox.shape, 
                [[xmin,ymin],[xmax,ymin],[xmax,ymax]],
                image.shape)
    return img_boundbox