import cv2
import numpy as np

def fix_boundbox(bndbox_img, bndbox_shape, img_pts, img_shape):
    bndbox_height, bndbox_width = bndbox_shape[:2]
    img_height, img_width = img_shape[:2]
    in_pts = np.float32([[0,0], [bndbox_width-1,0], [bndbox_width-1,bndbox_height-1]])
    out_pts = np.float32(img_pts)
    M = cv2.getAffineTransform(in_pts, out_pts)
    fix_img = cv2.warpAffine(bndbox_img, M, (img_width,img_height))

    return fix_img

def __bndbox_manager(image, label_file, *args):

    var = args[0]
    bndbox_type = args[3]
    modules = args[4]

    bndboxes, points, label_data = label_file.get_objects_data()
    [width, height] = label_file.get_image_data()[:2]

    for bndbox in bndboxes:
        [xmin, ymin, xmax, ymax] = bndbox

        mask = np.zeros([height,width], dtype=np.uint8)
        poly = cv2.fillConvexPoly(mask, np.int32([[xmin,ymin],[xmax,ymin],[xmax,ymax],[xmin,ymax]]), (255, 255, 255), cv2.LINE_AA)
        img_boundbox = image[ymin:ymax,xmin:xmax,:]

        img_modif = modules[bndbox_type](image, img_boundbox, var, [xmin,ymin, xmax,ymax])
        image[np.where(mask==255)] = img_modif[np.where(mask==255)]

    return image, label_file