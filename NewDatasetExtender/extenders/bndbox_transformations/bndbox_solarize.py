import cv2
import random
import numpy as np
from .__bndbox_manager import fix_boundbox

def bndbox_solarize(image, img_boundbox, var, bndbox, *args):
    [xmin, ymin, xmax, ymax] = bndbox

    solarization_const = random.uniform(1,3) * np.pi / 255
    look_up_table = np.ones((256, 1), dtype = 'uint8' ) * 0
    for i in range(256):
        look_up_table[i][0] = np.abs(np.sin(i * solarization_const)) * 100

    img_boundbox = cv2.LUT(img_boundbox, look_up_table)

    img_boundbox = fix_boundbox(img_boundbox, img_boundbox.shape, 
                [[xmin,ymin],[xmax,ymin],[xmax,ymax]],
                image.shape)
    return img_boundbox