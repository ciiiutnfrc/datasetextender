import cv2
import numpy as np
from .__bndbox_manager import fix_boundbox

neighbourhood4 = np.array([[0, 1, 0],
                        [1, 1, 1],
                        [0, 1, 0]],
                        np.uint8)
neighbourhood8 = np.array([[1, 1, 1],
                        [1, 1, 1],
                        [1, 1, 1]],
                        np.uint8)


def bndbox_dilation(image, img_boundbox, var, bndbox, *args):
    [xmin, ymin, xmax, ymax] = bndbox

    img_boundbox = cv2.dilate(img_boundbox, neighbourhood8, iterations=var)

    img_boundbox = fix_boundbox(img_boundbox, img_boundbox.shape, 
                [[xmin,ymin],[xmax,ymin],[xmax,ymax]],
                image.shape)
    return img_boundbox