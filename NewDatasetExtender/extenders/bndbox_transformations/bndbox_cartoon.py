import cv2
import numpy as np
from .__bndbox_manager import fix_boundbox

def bndbox_cartoon(image, img_boundbox, var, bndbox, *args):
    [xmin, ymin, xmax, ymax] = bndbox

    gray = cv2.cvtColor(img_boundbox, cv2.COLOR_BGR2GRAY)
    edges = cv2.adaptiveThreshold(np.uint8(gray), 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 9, 9)
    color = cv2.bilateralFilter(img_boundbox, 9, 200, 200)

    img_boundbox = cv2.bitwise_and(color, color, mask = edges)

    img_boundbox = fix_boundbox(img_boundbox, img_boundbox.shape, 
                [[xmin,ymin],[xmax,ymin],[xmax,ymax]],
                image.shape)
    return img_boundbox