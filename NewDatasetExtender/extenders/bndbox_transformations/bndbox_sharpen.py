import cv2
import numpy as np
from .__bndbox_manager import fix_boundbox
sharp_kernel = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])

def bndbox_sharpen(image, img_boundbox, var, bndbox, *args):
    [xmin, ymin, xmax, ymax] = bndbox

    img_boundbox = cv2.filter2D(img_boundbox, -1, sharp_kernel)

    img_boundbox = fix_boundbox(img_boundbox, img_boundbox.shape, 
                [[xmin,ymin],[xmax,ymin],[xmax,ymax]],
                image.shape)
    return img_boundbox