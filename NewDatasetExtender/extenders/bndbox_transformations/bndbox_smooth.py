import cv2
import numpy as np
from .__bndbox_manager import fix_boundbox

def bndbox_smooth(image, img_boundbox, var, bndbox, *args):
    [xmin, ymin, xmax, ymax] = bndbox

    img_boundbox = cv2.medianBlur(img_boundbox.astype(np.uint8),var)

    img_boundbox = fix_boundbox(img_boundbox, img_boundbox.shape, 
                [[xmin,ymin],[xmax,ymin],[xmax,ymax]],
                image.shape)
    return img_boundbox