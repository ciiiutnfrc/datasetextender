import cv2
from .__bndbox_manager import fix_boundbox

def bndbox_color_map(image, img_boundbox, var, bndbox, *args):
    [xmin, ymin, xmax, ymax] = bndbox

    if var>12:
        var=12
    if var<0:
        var=0

    img_boundbox = cv2.cvtColor(img_boundbox, cv2.COLOR_BGR2GRAY)
    img_boundbox = cv2.applyColorMap(img_boundbox, var)

    img_boundbox = fix_boundbox(img_boundbox, img_boundbox.shape, 
                [[xmin,ymin],[xmax,ymin],[xmax,ymax]],
                image.shape)
    return img_boundbox