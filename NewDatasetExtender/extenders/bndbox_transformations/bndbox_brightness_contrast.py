import cv2
import random
import numpy as np
from .__bndbox_manager import fix_boundbox

def bndbox_brightness_contrast(image, img_boundbox, var, bndbox, *args):
    [xmin, ymin, xmax, ymax] = bndbox

    try:
        alpha = random.uniform(1.0,3.0)
        beta = random.randrange(20,80)
    except ValueError:
        print('Error, not a number')

    img_boundbox = cv2.convertScaleAbs(img_boundbox, alpha=alpha, beta=beta)    

    img_boundbox = fix_boundbox(img_boundbox, img_boundbox.shape, 
                [[xmin,ymin],[xmax,ymin],[xmax,ymax]],
                image.shape)
    return img_boundbox