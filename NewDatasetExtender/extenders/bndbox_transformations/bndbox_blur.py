import cv2
from .__bndbox_manager import fix_boundbox

def bndbox_blur(image, img_boundbox, var, bndbox, *args):
    [xmin, ymin, xmax, ymax] = bndbox

    ksize=(var,var)
    img_boundbox = cv2.blur(img_boundbox, ksize)

    img_boundbox = fix_boundbox(img_boundbox, img_boundbox.shape, 
                [[xmin,ymin],[xmax,ymin],[xmax,ymax]],
                image.shape)
    return img_boundbox