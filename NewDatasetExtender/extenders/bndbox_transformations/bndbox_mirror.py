import cv2
from .__bndbox_manager import fix_boundbox

def bndbox_mirror(image, img_boundbox, var, bndbox, *args):
    [xmin, ymin, xmax, ymax] = bndbox

    var = var-1
    if var > 1:
        print("Error, mirror types available are 0=both, 1=vertical, 2=horizontal")
        return None
    else:
        if var==-1:
            flipx=True
            flipy=True
        elif var==0:
            flipx=False
            flipy=True
        elif var==1:
            flipx=True
            flipy=False

    img_boundbox = cv2.flip(img_boundbox, var)

    img_boundbox = fix_boundbox(img_boundbox, img_boundbox.shape, 
                [[xmin,ymin],[xmax,ymin],[xmax,ymax]],
                image.shape)
    return img_boundbox