import cv2
import random
import numpy as np
from .__bndbox_manager import fix_boundbox

def image_translation(image):
    '''
        This method recieves an image and returns the image
        with a random perspective resize warp
        The output image size and the shape of the data will
        be the same, but the image content will be in a smaller region
    '''
    height,width = image.shape[:2]
    in_pts = np.float32([[0,0], [width-1,0], [width-1,height-1]])

    x_translation = random.randint(-width//2, width//2)
    y_translation = random.randint(-height//2, height//2)

    out_pts = np.float32([[0+x_translation, y_translation],
                        [width-1+x_translation, y_translation],
                        [width-1+x_translation, height-1+y_translation]])
                        
    M=cv2.getAffineTransform(in_pts,out_pts)
    image = cv2.warpAffine(image, M, (width, height))
    return image

def bndbox_translate(image, img_boundbox, var, bndbox, *args):
    [xmin, ymin, xmax, ymax] = bndbox

    img_boundbox = image_translation(img_boundbox)

    img_boundbox = fix_boundbox(img_boundbox, img_boundbox.shape, 
                [[xmin,ymin],[xmax,ymin],[xmax,ymax]],
                image.shape)
    return img_boundbox