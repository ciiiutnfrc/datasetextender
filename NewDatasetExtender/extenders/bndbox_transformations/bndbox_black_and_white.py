import cv2
import numpy as np
from .__bndbox_manager import fix_boundbox

def bndbox_black_and_white(image, img_boundbox, var, bndbox, *args):
    [xmin, ymin, xmax, ymax] = bndbox

    img_gry = cv2.cvtColor(img_boundbox, cv2.COLOR_BGR2GRAY)
    img_boundbox = cv2.merge((img_gry,img_gry,img_gry)) 

    img_boundbox = fix_boundbox(img_boundbox, img_boundbox.shape, 
                [[xmin,ymin],[xmax,ymin],[xmax,ymax]],
                image.shape)
    return img_boundbox