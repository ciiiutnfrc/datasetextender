import cv2
import numpy as np
from .__bndbox_manager import fix_boundbox

size = 15
kernel_motion_blur = np.zeros((size, size))
kernel_motion_blur[int((size-1)/2), :] = np.ones(size)
kernel_motion_blur = kernel_motion_blur / size

def bndbox_motion_blur(image, img_boundbox, var, bndbox, *args):
    [xmin, ymin, xmax, ymax] = bndbox

    img_boundbox = cv2.filter2D(img_boundbox, -1, kernel_motion_blur)

    img_boundbox = fix_boundbox(img_boundbox, img_boundbox.shape, 
                [[xmin,ymin],[xmax,ymin],[xmax,ymax]],
                image.shape)
    return img_boundbox