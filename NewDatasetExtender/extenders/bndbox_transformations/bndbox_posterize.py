import cv2
import numpy as np
from .__bndbox_manager import fix_boundbox

def bndbox_posterize(image, img_boundbox, var, bndbox, *args):
    [xmin, ymin, xmax, ymax] = bndbox

    n = 2**var
    indices = np.arange(0,256)
    divider = np.linspace(0,255,n+1)[1]
    quantiz = np.int0(np.linspace(0,255,n))
    color_levels = np.clip(np.int0(indices/divider),0,n-1)
    palette = quantiz[color_levels]
    im2 = palette[img_boundbox.astype(int)]
    img_boundbox = cv2.convertScaleAbs(im2)

    img_boundbox = fix_boundbox(img_boundbox, img_boundbox.shape, 
                [[xmin,ymin],[xmax,ymin],[xmax,ymax]],
                image.shape)
    return img_boundbox