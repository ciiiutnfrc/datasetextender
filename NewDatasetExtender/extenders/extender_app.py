import xml.etree.ElementTree as ET 
import os
import re
import cv2
import math
import extenders.img_transformations
import extenders.bndbox_transformations
from .save_files import File_Manager, create_folders, save_labels
import random

output_count = 0

class Extender():
    __temppath = '/'.join(re.sub(r'\\', '/', os.path.abspath(__file__)).split('/')[:-1])
    ALL_MODULES = []
    for __tempmod in os.listdir(os.path.dirname(__temppath+'/img_transformations/')):
        if __tempmod == '__init__.py' or __tempmod[-3:] != '.py':
            continue
        if __tempmod[:2] == '__':
            continue
        ALL_MODULES.append(__tempmod[:-3])
    for __tempmod in os.listdir(os.path.dirname(__temppath+'/bndbox_transformations/')):
        if __tempmod == '__init__.py' or __tempmod[-3:] != '.py':
            continue
        if __tempmod[:2] == '__':
            continue
        ALL_MODULES.append(__tempmod[:-3])
    ALL_MODULES.sort()
        

    BILATERAL = "bilateral"
    BLACK_AND_WHITE = "black_and_white"
    BLUR = "blur"
    BNDBOX_BILATERAL = "bndbox_bilateral"
    BNDBOX_BLACK_AND_WHITE = "bndbox_black_and_white"
    BNDBOX_BLUR = "bndbox_blur"
    BNDBOX_BRIGHTNESS_CONTRAST = "bndbox_brightness_contrast"
    BNDBOX_CARTOON = "bndbox_cartoon"
    BNDBOX_CLAHE = "bndbox_clahe"
    BNDBOX_COLOR_INVERT = "bndbox_color_invert"
    BNDBOX_COLOR_MAP = "bndbox_color_map"
    BNDBOX_DILATION = "bndbox_dilation"
    BNDBOX_EROSION = "bndbox_erosion"
    BNDBOX_MIRROR = "bndbox_mirror"
    BNDBOX_MOTION_BLUR = "bndbox_motion_blur"
    BNDBOX_NOISE = "bndbox_noise"
    BNDBOX_POSTERIZE = "bndbox_posterize"
    BNDBOX_ROTATE = "bndbox_rotate"
    BNDBOX_SHARPEN = "bndbox_sharpen"
    BNDBOX_SMOOTH = "bndbox_smooth"
    BNDBOX_SOLARIZE = "bndbox_solarize"
    BNDBOX_TRANSLATE = "bndbox_translate"
    BRIGHTNESS_CONTRAST = "brightness_contrast"
    CARTOON = "cartoon"
    CLAHE = "clahe"
    COLOR_FILTER = "color_filter"
    COLOR_INVERT = "color_invert"
    COLOR_MAP = "color_map"
    COLOR_PSEUDO = "color_pseudo"
    CUT_MIX = "cut_mix"
    DILATION = "dilation"
    EDGE = "edge"
    EQUALIZE = "equalize"
    EROSION = "erosion"
    MIRROR = "mirror"
    MIXUP = "mixup"
    MOSAIC = "mosaic"
    MOTION_BLUR = "motion_blur"
    NOISE = "noise"
    OCCLUSION_HNS = "occlusion_hns"
    OCCLUSION_MASK = "occlusion_mask"
    OCCLUSION_RANDOM = "occlusion_random"
    PERSPECTIVE = "perspective"
    POSTERIZE = "posterize"
    RAIN = "rain"
    RESIZE = "resize"
    ROTATE = "rotate"
    SCALE = "scale"
    SHARPEN = "sharpen"
    SMOOTH = "smooth"
    SOLARIZE = "solarize"
    TRANSLATE = "translate"

    __ranges = {
            "blur": [5,11],
            "color": [0,11],
            "dilation": [1,4],
            "erosion": [1,4],
            "mirror": [0,2],
            "mosaic": [1,math.inf],
            "occlusion_mask": [1,math.inf],
            "posterize": [1,4],
            "rotate": [1,math.inf],
            "scale": [0,2],
            "smooth": [3,9],
        }
    
    __gen_ranges = {
            "blur": [5,11,1],
            "color": [0,11,1],
            "dilation": [1,4,1],
            "erosion": [1,4,1],
            "mirror": [0,2,1],
            "mosaic": [1,30,1],
            "occlusion_mask": [1,100,1],
            "posterize": [1,4,1],
            "rotate": [-360,360,1],
            "scale": [0,2,1],
            "smooth": [3,9,2],
        }

    def __init__(self, images, xml_output:bool, txt_output:bool):
        self.images = images
        self.xml_output = xml_output
        self.txt_output = txt_output

        if len(images)<=0:
            print("Error, no images in folder")

        self.modules={}

        temppath = re.sub(r'\\', '/', os.path.abspath(__file__))
        temppath = '/'.join(temppath.split('/')[:-1])
        for module in os.listdir(os.path.dirname(temppath+'/img_transformations/')):
            if module == '__init__.py' or module[-3:] != '.py':
                continue
            if module[:2] == '__':
                continue
            
            self.modules[module[:-3]]=getattr(extenders.img_transformations,module[:-3])

        self.modules["bndbox_manager"]=getattr(extenders.bndbox_transformations,'__bndbox_manager')

        self.bndbox_modules = {}
        for module in os.listdir(os.path.dirname(temppath+'/bndbox_transformations/')):
            if module == '__init__.py' or module[-3:] != '.py':
                continue
            if module[:2] == '__':
                continue
            
            self.bndbox_modules[module[:-3]]=getattr(extenders.bndbox_transformations,module[:-3])


    def extend(self, 
                output_folder:str, folder_tag:str, file_tag:str,
                transformations_list,

                blur:int = -1,
                color_map:int = -1,
                dilation:int = -1,
                erosion:int = -1,
                mirror:int = -1,
                mosaic:int = -1,
                occlusion_mask:int = -1,
                posterize:int = -1,
                rotate:float = -1,
                scale:int = -1,
                smooth:int = -1,
                **kwargs):
        global output_count
        
        values = {
            "blur": blur,
            "color_map": color_map,
            "dilation": dilation,
            "erosion": erosion,
            "mosaic": mosaic,
            "mirror": mirror,
            "occlusion_mask": occlusion_mask,
            "posterize": posterize,
            "rotate": rotate,
            "scale": scale,
            "smooth": smooth
        }
        if len(kwargs)>0:
            values.update(kwargs)

        try:
            os.stat(output_folder)
        except:
            os.mkdir(output_folder)
        
        if (folder_tag is None) or (folder_tag==""):
            folder_tag = 'extend-id{0:04}'.format(round(output_count,4))
            output_count += 1

        if (file_tag is None) or (file_tag==""):
            file_tag = folder_tag

        if len(self.images)<=0:
            print("Error, no images in folder")
        else:
            create_folders(self.images[0], output_folder, folder_tag)

            for file_num in range(0,len(self.images)):
                f_name = re.sub(r'\\', '/', os.path.abspath(self.images[file_num]))
                image, file = self.create_data_vector(file_num, self.xml_output, self.txt_output)
                if file is None:
                    continue

                if any(item in ['mixup','cut_mix','rotate'] for item in transformations_list):
                    image2, file2 = self.create_data_vector(min(file_num+1,len(self.images)-1), self.xml_output, self.txt_output)
                    if file2 is None:
                        continue
                else:
                    image2, file2 = None, None
                
                for item in transformations_list:
                    if item in self.modules:
                        if item in values:
                            var = values[item]
                            if (item in self.__ranges):
                                if (var not in range(self.__gen_ranges[item][0],self.__gen_ranges[item][1]+1,self.__gen_ranges[item][2])) or (var is None):
                                    var = random.randrange(self.__gen_ranges[item][0],self.__gen_ranges[item][1]+1,self.__gen_ranges[item][2]) 
                        else:
                            var = None
                        
                        image, file = self.modules[item](image, file, var, image2, file2)
                    
                    elif item.startswith("bndbox_"):
                        bndbox_item = item[7:]
                        if bndbox_item in values:
                            var = values[bndbox_item]
                            if (bndbox_item in self.__ranges):
                                if (var not in range(self.__gen_ranges[bndbox_item][0],self.__gen_ranges[bndbox_item][1]+1,self.__gen_ranges[bndbox_item][2])) or (var is None):
                                    var = random.randrange(self.__gen_ranges[bndbox_item][0],self.__gen_ranges[bndbox_item][1]+1,self.__gen_ranges[bndbox_item][2])
                        else:
                            var = None

                        image, file = self.modules["bndbox_manager"](image, file, var, image2, file2, item, self.bndbox_modules)
                
                folder_name=f_name.split('/')[-2] + f'-{folder_tag}'
                file.save_files(output_folder, folder_name, file_tag)
                cv2.imwrite(output_folder + '/' + f_name.split('/')[-2] + f'-{folder_tag}/' + f'{file_tag}_' + f_name.split('/')[-1], image)
                del file
                if file2 is not None:
                    del file2
            if self.txt_output:
                save_labels(self.images[0], output_folder, folder_tag)
            
                
    def create_data_vector(self, file_num, xml_output, txt_output):
        f_name = re.sub(r'\\', '/', os.path.abspath(self.images[file_num]))
        image = cv2.imread(f_name)
        if not os.path.exists(f_name) or not os:
            print("Error, path does not exist")
            return None, None
        if image is None:
            print(f'Error reading the image: "{f_name}" .\n')
            return None, None

        file = File_Manager(self.images[file_num], xml_output, txt_output)
        root = file.root
        if root is None:
            print(f'The file: "{f_name}" has no labels.\n'+
                    "Please label the image and give it the same name as the image's")
            del file
            return None, None

        return image, file

