import xml.etree.ElementTree as ET 
import os
import re
import cv2

all_labels = []

class File_Manager():
    def __init__(self, file_path:str, xml_output:bool, txt_output:bool):
        self.input_folder = re.sub(r'\\', '/', os.path.abspath(file_path))
        self.xml_output = xml_output
        self.txt_output = txt_output

        lbl_file='/'.join(self.input_folder.split('/')[:-1])+'/classes.txt'
        if os.path.exists(lbl_file):
            with open(lbl_file) as open_lbl:
                for lbl in open_lbl.readlines():
                    if lbl.endswith('\n'):
                        if not (lbl[:-1] in all_labels):
                            all_labels.append(lbl[:-1])
                    else:
                        if not (lbl in all_labels):
                            all_labels.append(lbl)

        self.root = None 

        name=file_path.split('.')
        if len(name)>1:
            name='.'.join(name[:-1])
        else:
            name=name[0]

        if os.path.exists(name+'.xml'):
            self.tree = ET.parse(name+'.xml')  
            self.root = self.tree.getroot()

            for object_xml in self.root.findall('object'):
                if not (object_xml.find('name').text in all_labels):
                    all_labels.append(object_xml.find('name').text)

        elif os.path.exists(name+'.txt'):
            self.root = ET.Element('annotation')
            self.tree = ET.ElementTree(self.root)

            with open(name+'.txt', 'r') as orig_f:
                
                folder_name = self.input_folder.split('/')[-2]
                file_name = self.input_folder.split('/')[-1]
                ET.SubElement(self.root, 'folder').text=folder_name
                ET.SubElement(self.root, 'filename').text=file_name
                ET.SubElement(self.root, 'path').text=file_path

                source = ET.SubElement(self.root, 'source')
                ET.SubElement(source, 'database').text = 'Unknown'

                image = cv2.imread(file_path)
                h,w,ch = image.shape
                size = ET.SubElement(self.root, 'size')
                ET.SubElement(size, 'width').text= str(w)
                ET.SubElement(size, 'height').text= str(h)
                ET.SubElement(size, 'depth').text= str(ch)

                ET.SubElement(self.root, 'segmented').text = '0'

                for line in orig_f.readlines():
                    if len(line)>0:
                        txt_data=line.split(' ')
                        obj_class, x_center, y_center, width, height = txt_data[:5]
                        obj = ET.SubElement(self.root, 'object')
                        if int(obj_class) < len(all_labels):
                            ET.SubElement(obj, 'name').text = all_labels[int(obj_class)]
                        else:
                            ET.SubElement(obj, 'name').text = 'UNKNOWN'
                        ET.SubElement(obj, 'pose').text = 'Unspecified'
                        ET.SubElement(obj, 'truncated').text = '0'
                        ET.SubElement(obj, 'difficut').text = '0'
                        bndbox = ET.SubElement(obj, 'bndbox')
                        ET.SubElement(bndbox, 'xmin').text = str(round((float(x_center) - float(width))*w))
                        ET.SubElement(bndbox, 'ymin').text = str(round((float(y_center) - float(height))*h))
                        ET.SubElement(bndbox, 'xmax').text = str(round((float(x_center) + float(width))*w))
                        ET.SubElement(bndbox, 'ymax').text = str(round((float(y_center) + float(height))*h))

                orig_f.close()
                self.indent(self.root)
    
    def get_image_data(self):
        file_data=[]
        
        size_xml = self.root.find('size')
        width_xml = int(size_xml.find('width').text)
        file_data.append(width_xml)
        height_xml = int(size_xml.find('height').text) 
        file_data.append(height_xml)

        return file_data
    
    def set_image_data(self, width, height):
        size_xml = self.root.find('size')
        size_xml.find('width').text = str(width)
        size_xml.find('height').text = str(height)


    def get_objects_data(self):
        label_data = []
        points = []

        for object_xml in self.root.findall('object'):
            bndbox_xml = object_xml.find('bndbox')
            
            temppts = []
            temppts.append(int(bndbox_xml.find('xmin').text))
            temppts.append(int(bndbox_xml.find('ymin').text))
            temppts.append(int(bndbox_xml.find('xmax').text))
            temppts.append(int(bndbox_xml.find('ymax').text))
            points.append(temppts)
            
            tempdata=[]
            tempdata.append(object_xml.find('name').text)
            tempdata.append(object_xml.find('pose').text)
            tempdata.append(object_xml.find('truncated').text)
            tempdata.append(object_xml.find('difficult').text)
            label_data.append(tempdata)
        
        '''
        En el caso futuro de agregar la funcionalidad de COCO, aqui deberiamos
        modificar que boundbox no sea igual que points, sino que consigo todos los points
        de cada objeto y luego de cada consigo el boundbox del objeto.
        Se hace asi para facilitar el cropeado de imagenes, aunque Coco ya te genera el boundbox
        en el annotation asi que eso lo facilita.
        '''
        boundboxes = points

        return boundboxes, points, label_data

    def add_object(self, label_data, points):

        name = label_data[0]
        pose = label_data[1]
        truncated = label_data[2]
        difficult = label_data[3]

        obj = ET.SubElement(self.root, 'object')
        ET.SubElement(obj, 'name').text = name
        ET.SubElement(obj, 'pose').text = pose
        ET.SubElement(obj, 'truncated').text = truncated
        ET.SubElement(obj, 'difficult').text = difficult
        bndbox = ET.SubElement(obj, 'bndbox')
        ET.SubElement(bndbox, 'xmin').text = str(points[0])
        ET.SubElement(bndbox, 'ymin').text = str(points[1])
        ET.SubElement(bndbox, 'xmax').text = str(points[2])
        ET.SubElement(bndbox, 'ymax').text = str(points[3])

        self.indent(self.root)

    def remove_all_objects(self):
        for object_xml in self.root.findall('object'):
            self.root.remove(object_xml)

    def indent(self, elem, level=0):
        i = "\n" + level*"    "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "    "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for elem in elem:
                self.indent(elem, level+1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i

    def save_files(self, output_folder, folder_name, file_tag):
        # Guardo los TXT
        if self.txt_output:
            fullname=self.root.find('filename').text
            name=fullname.split('.')
            if len(name)>1:
                name='.'.join(name[:-1])
            else:
                name=name[0]
            
            new_txt=output_folder + '/' + folder_name + '/' + f'{file_tag}_' + name + '.txt'
            
            with open(new_txt, 'w') as f:
                objects = self.root.findall('object')
                for obj in objects:
                    bndbox_xml = obj.find('bndbox')
                    if obj.find('name').text in all_labels:
                        object_class = str (all_labels.index(obj.find('name').text))
                    else:
                        object_class='404'
                    xmin=int(bndbox_xml.find('xmin').text)
                    xmax=int(bndbox_xml.find('xmax').text)
                    ymin=int(bndbox_xml.find('ymin').text)
                    ymax=int(bndbox_xml.find('ymax').text)
                    img_width=int(self.root.find('size').find('width').text)
                    img_height=int(self.root.find('size').find('height').text)

                    x_center=str((xmin+abs(xmax-xmin)/2)/img_width)
                    y_center=str((ymin+abs(ymax-ymin)/2)/img_height)
                    width=str(abs(xmax-xmin+abs(xmax-xmin)/2)/img_width)
                    height=str(abs(ymax-ymin+abs(ymax-ymin)/2)/img_height)

                    write_data=' '.join([object_class, x_center, y_center, width, height, '\n'])
                    f.write(write_data)

                f.close()

        # Guardo los XML
        if self.xml_output:
            self.root.find('filename').text = f'{file_tag}_' + self.root.find('filename').text
            self.root.find('folder').text = self.root.find('folder').text + f'-{folder_name}'  

            fullname=self.root.find('filename').text
            name=fullname.split('.')
            if len(name)>1:
                name='.'.join(name[:-1])
            else:
                name=name[0]
            ext=fullname.split('.')[-1]

            self.root.find('path').text = output_folder + '/' + folder_name + '/' + name + '.' + ext
            self.tree.write(output_folder + '/' + folder_name + '/' + name + '.xml')

def save_labels(image, output_folder, folder_tag):
    global all_labels
    input_folder = re.sub(r'\\', '/', os.path.abspath(image))
    
    if len(all_labels)>0:
        folder_name = input_folder.split('/')[-2]
        classes_txt=output_folder + f'/{folder_name}-{folder_tag}' + '/classes.txt'
        with open(classes_txt, 'w') as f:
            write_data = '\n'.join(all_labels)
            f.write(write_data)
    
    all_labels=[]
    

def create_folders(image, output_folder, folder_tag):
    temppath = re.sub(r'\\', '/', os.path.abspath(image))
    folder_name = temppath.split('/')[-2]
    create_folder = output_folder + f'/{folder_name}-{folder_tag}'
    try:
        os.stat(create_folder)
    except:
        os.mkdir(create_folder)
    
    return create_folder
