import cv2
import random

def color_filter(image, label_file, *args):
    img = image
        
    img_bgr = cv2.split(img)
    '''img_cng = cv2.merge(
                    (img_bgr[0] * random.randint(0,3),
                    img_bgr[1] * random.randint(0,3),
                    img_bgr[2] * random.randint(0,3)))'''
    img_r = cv2.merge(
                    (img_bgr[0] * 0,
                    img_bgr[1] * 0,
                    img_bgr[2] * 1))
    img_g = cv2.merge(
                    (img_bgr[0] * 0,
                    img_bgr[1] * 1,
                    img_bgr[2] * 0))
    img_b = cv2.merge(
                    (img_bgr[0] * 1,
                    img_bgr[1] * 0,
                    img_bgr[2] * 0))
    img_temp = cv2.addWeighted(img_r, random.uniform(0,1), img_g, random.uniform(0,1), 0)
    img_cng = cv2.addWeighted(img_temp, 1, img_b, random.uniform(0,1), 0)
    
    return img, label_file