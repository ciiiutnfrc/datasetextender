import cv2
import random
import numpy as np

look_up_table_r = np.ones((256, 1), dtype = 'uint8' ) * 0
look_up_table_g = np.ones((256, 1), dtype = 'uint8' ) * 0
look_up_table_b = np.ones((256, 1), dtype = 'uint8' ) * 0

def get_lookuptable(x,r1,r2,r3,r4):
    
    if x < 86:
        return x * r1 / r2
    elif x < 172:
        return x * r2 / r3
    else:
        return x * r3 / r4


def color_pseudo(image, label_file, *args):
    img = image

    r1=random.randint(1,10)
    r2=random.randint(1,10)
    r3=random.randint(1,10)
    r4=random.randint(1,10)

    for i in range(256):
        look_up_table_r[i][0] = get_lookuptable(i,r1,r2,r3,r4)
        look_up_table_g[i][0] = get_lookuptable(i,r4,r3,r2,r1)
        look_up_table_b[i][0] = get_lookuptable(i,r2,r4,r1,r3)


    img_gry = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    img_pcp_r = cv2.LUT(img_gry, look_up_table_r)
    img_pcp_g = cv2.LUT(img_gry, look_up_table_g)
    img_pcp_b = cv2.LUT(img_gry, look_up_table_b)

    img_mrg = cv2.merge((img_pcp_b,img_pcp_g,img_pcp_r)) 
    
    return img_mrg, label_file