import cv2
import numpy as np
import random

def solarize(image, label_file, *args):

    solarization_const = random.uniform(1,3) * np.pi / 255
    look_up_table = np.ones((256, 1), dtype = 'uint8' ) * 0

    for i in range(256):
        look_up_table[i][0] = np.abs(np.sin(i * solarization_const)) * 100

    img_sola = cv2.LUT(image, look_up_table)

    return img_sola, label_file