import cv2

def mirror(image, label_file, *args):
    mirror_type = args[0]

    mirror_type-=1

    if mirror_type > 1:
        print("Error, mirror types available are 0=both, 1=vertical, 2=horizontal")
        return None
    else:
        if mirror_type==-1:
            flipx=True
            flipy=True
        elif mirror_type==0:
            flipx=False
            flipy=True
        elif mirror_type==1:
            flipx=True
            flipy=False

        img = cv2.flip(image, mirror_type)

        [width, height] = label_file.get_image_data()[:2]
        bndboxes, point, label_data = label_file.get_objects_data()
        label_file.remove_all_objects()
        for i, bndbox in enumerate(bndboxes):
            [xmin, ymin, xmax, ymax] = bndbox

            if flipx:
                espacio_al_borde_x = width - xmax
                ancho_bouding_box = xmax - xmin

                xmin = espacio_al_borde_x
                xmax = espacio_al_borde_x + ancho_bouding_box

            if flipy:
                espacio_al_borde_y = height - ymax
                alto_bouding_box = ymax - ymin
                
                ymin  = espacio_al_borde_y
                ymax = espacio_al_borde_y + alto_bouding_box

            boundbox_pts = [xmin,ymin,xmax,ymax]
            
            label_file.add_object(label_data[i], boundbox_pts)
            
    return img, label_file