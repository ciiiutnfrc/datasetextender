import cv2
import numpy as np

#Recommended range [1 - 5]

neighbourhood4 = np.array([[0, 1, 0],
                    [1, 1, 1],
                    [0, 1, 0]],
                    np.uint8)
neighbourhood8 = np.array([[1, 1, 1],
                        [1, 1, 1],
                        [1, 1, 1]],
                        np.uint8)

def erosion(image, label_file, *args):
    erosion_level = args[0]

    img_erosion = cv2.erode(image, neighbourhood8, iterations=erosion_level)
    
    return img_erosion, label_file