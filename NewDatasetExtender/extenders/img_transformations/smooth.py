import cv2
import numpy as np

#Recommended range [3 - 9] (Odd numbers)

def smooth(image, label_file, *args):
    smooth_level = args[0]

    img = cv2.medianBlur(image.astype(np.uint8),smooth_level)
    
    return img, label_file