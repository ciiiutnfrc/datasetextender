import cv2
import numpy as np

def bilateral(image, label_file, *args):

    #average_square_size = 25
    #sigma_color = 75
    #sigma_metric = 75

    img = cv2.bilateralFilter(image.astype(np.uint8), 25, 75, 75)

    return img, label_file