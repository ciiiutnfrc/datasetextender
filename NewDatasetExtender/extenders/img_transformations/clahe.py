import cv2
import numpy as np

def clahe(image, label_file, *args):

    (b, g, r) = cv2.split(image)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    b = clahe.apply(b)
    g = clahe.apply(g)
    r = clahe.apply(r)
    image = cv2.merge((b,g,r))
    
    return image, label_file