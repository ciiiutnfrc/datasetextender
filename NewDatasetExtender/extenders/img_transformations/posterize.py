import cv2
import numpy as np

#Recommended ranges [1 - 3]

def posterize(image, label_file, *args):
    post_level = args[0]

    n = 2**post_level  # Number of levels of quantization
    indices = np.arange(0,256)   # List of all colors 
    divider = np.linspace(0,255,n+1)[1] # we get a divider
    quantiz = np.int0(np.linspace(0,255,n)) # we get quantization colors
    color_levels = np.clip(np.int0(indices/divider),0,n-1) # color levels 0,1,2..
    palette = quantiz[color_levels] # Creating the palette
    im2 = palette[image.astype(int)]  # Applying palette on image
    im2 = cv2.convertScaleAbs(im2) # Converting image back to uint8

    return im2, label_file