import cv2

#Recomended range [5 - 11]

def blur(image, label_file, *args):
    blur_level = args[0]
    img = image
        
    
    ksize=(blur_level,blur_level)
    img = cv2.blur(img, ksize)

    #ksize(random.randrange(5, 15+1, 2), random.randrange(5, 15+1, 2))
    #img = cv2.GaussianBlur(img,ksize,0)

    #ksize(random.randrange(5, 15+1, 2), random.randrange(5, 15+1, 2))
    #img = cv2.medianBlur(img,ksize[0])

    return img, label_file