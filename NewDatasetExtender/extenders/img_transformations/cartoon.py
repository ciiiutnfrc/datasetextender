import cv2
import numpy as np

def cartoon(image, label_file, *args):

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    edges = cv2.adaptiveThreshold(np.uint8(gray), 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 9, 9)
    color = cv2.bilateralFilter(image, 9, 200, 200)

    cartoon = cv2.bitwise_and(color, color, mask = edges)
    
    return cartoon, label_file