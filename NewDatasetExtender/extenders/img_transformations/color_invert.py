import cv2
import numpy as np

def color_invert(image, label_file, *args):

    image = np.invert(image)
    
    return image, label_file