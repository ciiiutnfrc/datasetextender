
import cv2
import numpy as np
import random

def apply_mask(image, size:int):
    '''
        This method recieves an image and the size of the occlusion
        squares, and returns an image with mask occlusion.
        If no size its given, in the main function we generate a
        random size based on the image shape.
    '''
    row,col,ch= image.shape
    mean = -0.5
    var = 1.5
    sigma = var**3
    gauss = np.random.normal(mean,sigma,(row,col,ch))
    cv2.resize(gauss, (col, row))

    blackimg = np.zeros([row,col,ch],dtype=np.uint8)
    grayimg = np.zeros([row,col,ch],dtype=np.uint8)
    grayimg.fill(126)
    noisy = cv2.addWeighted(blackimg, 0.0001, gauss, 0.99, 0.1, dtype=cv2.CV_32F)

    gray = cv2.cvtColor(noisy, cv2.COLOR_BGR2GRAY)
    gray_three = cv2.merge([gray,gray,gray])

    rand_box={0:blackimg,1:grayimg,2:noisy,3:gray_three}
    

    mask = np.zeros([row,col], dtype=np.uint8)
    for i in range(1, int(row/size)+1, 2):
        start_y= i * size
        for j in range(1, int(col/size)+1, 2):
            start_x= j * size
            pts_o = [[start_x,start_y],
                        [min(col, start_x+size), start_y],
                        [min(col, start_x+size), min(row, start_y+size)],
                        [start_x, min(row, start_y+size)]
                        ]
            
            cv2.fillConvexPoly(mask, np.int32([pts_o]), (255, 255, 255), cv2.LINE_AA)

    image[np.where(mask == 255)] = rand_box[random.randint(0,3)][np.where(mask == 255)]

    return image

def occlusion_mask(image, label_file, *args):
    input_size = args[0]

    img = image
    height, width = img.shape[:2]
    
    if input_size is not None:
        input_size = int(round(input_size))

    if (not input_size) or (input_size is None) or (input_size<=0):
        size = int((height*width/random.randint(100,300))**(1/2))
    else:
        size = input_size


    img = apply_mask(img, size)

    return img, label_file
