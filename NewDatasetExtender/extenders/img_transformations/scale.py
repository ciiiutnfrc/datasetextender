import cv2
import random
import math

def scale(image, label_file, *args):
    scale_type = args[0]
    if scale_type > 2:
        print("Error, mirror types available are 0=both, 1=vertical, 2=horizontal")
        return None
    else:
        if scale_type==0:
            hor_scale=True
            ver_scale=True
        elif scale_type==1:
            hor_scale=False
            ver_scale=True
        elif scale_type==2:
            hor_scale=True
            ver_scale=False

    img = image
    
    [width, height] = label_file.get_image_data()[:2]
    
    xmax_temp = 0
    xmin_temp = width
    ymax_temp = 0
    ymin_temp = height

    bndboxes, points, label_data = label_file.get_objects_data()
    
    for i, bndbox in enumerate(bndboxes):
        [xmin, ymin, xmax, ymax] = bndbox

        if xmin < xmin_temp :
            xmin_temp = xmin
        if xmax > xmax_temp :
            xmax_temp = xmax
        if ymin < ymin_temp :
            ymin_temp = ymin
        if ymax > ymax_temp :
            ymax_temp = ymax
    
    # Image cropping
    crop_img = img[ymin_temp:ymax_temp, xmin_temp:xmax_temp]

    # Scaling
    if hor_scale:
        scale_percent_x = random.randrange(110,300) # percent of original size
    else:
        scale_percent_x = 100
    
    if ver_scale:
        scale_percent_y = random.randrange(110,300) # percent of original size
    else:
        scale_percent_y = 100

    new_width = max(1,math.ceil(crop_img.shape[1] * scale_percent_x / 100))
    new_height = max(1,math.ceil(crop_img.shape[0] * scale_percent_y / 100))
    dim = (new_width, new_height)

    # resize image
    if crop_img.shape[0]*crop_img.shape[1] > 0:
        krop = cv2.resize(crop_img, dim)

        label_file.set_image_data(new_width, new_height)

        label_file.remove_all_objects()
        for i, bndbox in enumerate(bndboxes):
            [xmin, ymin, xmax, ymax] = bndbox

            new_x_min = xmin - xmin_temp
            new_x_max = xmax - xmin_temp
            new_y_min = ymin - ymin_temp
            new_y_max = ymax - ymin_temp

            new_x_min = int(new_x_min * scale_percent_x / 100)
            new_y_min = int(new_y_min * scale_percent_y / 100)
            new_x_max = int(new_x_max * scale_percent_x / 100)
            new_y_max = int(new_y_max * scale_percent_y / 100)

            label_file.add_object(label_data[i], [new_x_min,new_y_min, new_x_max,new_y_max])
            
        label_file.set_image_data(new_width, new_height)

        return krop, label_file
    
    return image, label_file