import cv2

def edge(image, label_file, *args):

        img = cv2.Canny(image, 60, 60)

        img_mrg = cv2.merge((img,img,img)) 

        return img_mrg, label_file