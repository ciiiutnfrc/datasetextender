import cv2
import numpy as np

size = 15
kernel_motion_blur = np.zeros((size, size))
kernel_motion_blur[int((size-1)/2), :] = np.ones(size)
kernel_motion_blur = kernel_motion_blur / size

def motion_blur(image, label_file, *args):
    
    image = cv2.filter2D(image, -1, kernel_motion_blur)

    return image, label_file