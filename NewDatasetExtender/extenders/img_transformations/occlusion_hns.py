import cv2
import numpy as np
import random

def rand_noise_hns(image, times:int):
    '''
        This method recieves an image and how many occlusion
        squares to add, and returns an image with random oclussion
    '''
    row,col,ch= image.shape
    mean = -0.5
    var = 1.5
    sigma = var**3
    gauss = np.random.normal(mean,sigma,(row,col,ch))
    cv2.resize(gauss, (col, row))

    blackimg = np.zeros([row,col,ch],dtype=np.uint8)
    grayimg = np.zeros([row,col,ch],dtype=np.uint8)
    grayimg.fill(126)
    noisy = cv2.addWeighted(blackimg, 0.0001, gauss, 0.99, 0.1, dtype=cv2.CV_32F)

    gray = cv2.cvtColor(noisy, cv2.COLOR_BGR2GRAY)
    gray_three = cv2.merge([gray,gray,gray])

    [minx, miny] = [0,0]
    [maxx, maxy] = [col-1,row-1]

    rand_box={0:blackimg,1:grayimg,2:noisy,3:gray_three}
    
    while times>0:
        rndpts = [[random.randint(minx,maxx),random.randint(miny,maxy)],
            [random.randint(minx,maxx),random.randint(miny,maxy)]]
        rndpts_o = [[rndpts[0][0],rndpts[0][1]],
                [rndpts[1][0],rndpts[0][1]],
                [rndpts[1][0],rndpts[1][1]],
                [rndpts[0][0],rndpts[1][1]]]
        rndpts_area = (abs(rndpts[0][0]-rndpts[1][0])*abs(rndpts[0][1]-rndpts[1][1]))
        if rndpts_area > row*col/16:
            continue
        
        mask = np.zeros([row,col], dtype=np.uint8)
        cv2.fillConvexPoly(mask, np.int32([rndpts_o]), (255, 255, 255), cv2.LINE_AA)
        times-=1

        image[np.where(mask == 255)] = rand_box[random.randint(0,3)][np.where(mask == 255)]

    return image

def occlusion_hns(image, label_file, *args):

    img = rand_noise_hns(image, 6)

    return img, label_file

