import cv2
import numpy as np
import math
from random import randint

def image_perspective(image, boundboxes):
    '''
        This function recieves an image and returns an
        image with a random perspective warp
    '''
    height,width,ch= image.shape
    in_pts = np.float32([[0,0], [width-1,0], [width-1,height-1], [0,height-1]])

    [xmin, ymin, xmax, ymax] = boundboxes

    out_pts = np.float32([[randint(-int(xmin*1.2),width//3), randint(-int(ymin*1.2),height//3)],
                        [randint(width*2//3,width+(width-int(xmax*0.8))), randint(-int(ymin*1.2),height//3)],
                        [randint(width*2//3,width+(width-int(xmax*0.8))), randint(height*2//3,height+(height-int(ymax*0.8)))],
                        [randint(-int(xmin*1.2),width//3), randint(height*2//3,height+(height-int(ymax*0.8)))]])
    M=cv2.getPerspectiveTransform(in_pts,out_pts)

    image = cv2.warpPerspective(image, M, (width, height))
    return image, M

def points_perspective(pts, rotation_mat):
    """
    Given a rotation matrix, rotates points in the image
    """

    xmin, xmax, ymin, ymax = pts[0], pts[1], pts[2], pts[3]
    tr_pts = np.float32([[xmin,ymin],[xmax,ymin],[xmin,ymax],[xmax,ymax]]).reshape(-1,1,2)

    rotated_points = cv2.perspectiveTransform(tr_pts, rotation_mat)

    out_min_x=math.inf
    out_max_x=-1
    out_min_y=math.inf
    out_max_y=-1

    for point in rotated_points:
        if point[0][0]>out_max_x:
            out_max_x=point[0][0]
        if point[0][0]<out_min_x:
            out_min_x=point[0][0]

        if point[0][1]>out_max_y:
            out_max_y=point[0][1]
        if point[0][1]<out_min_y:
            out_min_y=point[0][1]

    return int(out_min_x), int(out_max_x), int(out_min_y), int(out_max_y)


def perspective(image, label_file, *args):

    img = image

    [width, height] = label_file.get_image_data()[:2]
    xmax_temp = 0
    xmin_temp = width
    ymax_temp = 0
    ymin_temp = height

    bndboxes, points, label_data = label_file.get_objects_data()
    for bndbox in bndboxes:
        [xmin, ymin, xmax, ymax] = bndbox

        if xmin < xmin_temp :
            xmin_temp = xmin
        if xmax > xmax_temp :
            xmax_temp = xmax
        if ymin < ymin_temp :
            ymin_temp = ymin
        if ymax > ymax_temp :
            ymax_temp = ymax

    img, mtx = image_perspective(img, [xmin_temp, ymin_temp, xmax_temp, ymax_temp])

    label_file.remove_all_objects()
    
    for i, bndbox in enumerate(bndboxes):
        [xmin, ymin, xmax, ymax] = bndbox

        n_xmin, n_xmax, n_ymin, n_ymax = points_perspective([xmin,xmax,ymin,ymax], mtx)

        n_xmin = min(width,max(0,n_xmin))
        n_ymin = min(height,max(0,n_ymin))
        n_xmax = min(width,max(0,n_xmax))
        n_ymax = min(height,max(0,n_ymax))

        label_file.add_object(label_data[i], [n_xmin,n_ymin, n_xmax,n_ymax])
        
    return img, label_file