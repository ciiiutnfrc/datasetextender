import cv2
import numpy as np
sharp_kernel = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])

def sharpen(image, label_file, *args):

    image = cv2.filter2D(image, -1, sharp_kernel)
    
    return image, label_file