import cv2
import numpy as np

#Recommended range [1 - 5]

neighbourhood4 = np.array([[0, 1, 0],
                        [1, 1, 1],
                        [0, 1, 0]],
                        np.uint8)
neighbourhood8 = np.array([[1, 1, 1],
                        [1, 1, 1],
                        [1, 1, 1]],
                        np.uint8)

def dilation(image, label_file, *args):
    dilation_level = args[0]
    
    img_dilation = cv2.dilate(image, neighbourhood8, iterations=dilation_level)

    return img_dilation, label_file