import cv2
import numpy as np
import random

def rotate_points(pts, rotation_mat):
    """
        Given a rotation matrix, rotates points in the image
    """

    pt1, pt2, pt3, pt4 = pts[0], pts[1], pts[2], pts[3]
    tr_pts = np.float32([[pt1[0], pt1[1]],[pt2[0], pt2[1]],[pt3[0], pt3[1]],[pt4[0], pt4[1]]]).reshape(-1,1,2)

    rotated_points = cv2.transform(tr_pts, rotation_mat)

    return rotated_points

def rotate_image(mat, angle, pts):
    """
        Rotates an image (angle in degrees) and expands image to avoid cropping
    """

    height, width = mat.shape[:2] # image shape has 3 dimensions
    image_center = (width/2, height/2) # getRotationMatrix2D needs coordinates in reverse order (width, height) compared to shape

    rotation_mat = cv2.getRotationMatrix2D(image_center, angle, 1.)

    # rotation calculates the cos and sin, taking absolutes of those.
    abs_cos = abs(rotation_mat[0,0]) 
    abs_sin = abs(rotation_mat[0,1])

    # find the new width and height bounds
    bound_w = int(height * abs_sin + width * abs_cos)
    bound_h = int(height * abs_cos + width * abs_sin)

    # subtract old image center (bringing image back to origo) and adding the new image center coordinates
    rotation_mat[0, 2] += bound_w/2 - image_center[0]
    rotation_mat[1, 2] += bound_h/2 - image_center[1]

    # rotate image with the new bounds and translated rotation matrix
    rotated_mat = cv2.warpAffine(mat, rotation_mat, (bound_w, bound_h))
    out_pts = rotate_points(pts, rotation_mat)
    return rotated_mat, out_pts

def fix_boundbox_points(rotated_points):
    """
        Given a transformation matrix, fixes the points to the points where they should be
    """

    out_min_x=rotated_points[0][0][0]
    out_max_x=-1
    out_min_y=rotated_points[0][0][1]
    out_max_y=-1

    for point in rotated_points:
        if point[0][0]>out_max_x:
            out_max_x=point[0][0]
        if point[0][0]<out_min_x:
            out_min_x=point[0][0]

        if point[0][1]>out_max_y:
            out_max_y=point[0][1]
        if point[0][1]<out_min_y:
            out_min_y=point[0][1]

    return [int(out_min_x), int(out_min_y), int(out_max_x), int(out_max_y)]

def image_cut_mix(cut_img, orig_img):
    '''
        This method recieves an image and returns the image
        with a random perspective resize warp
        The output image size and the shape of the data will
        be the same, but the image content will be in a smaller region
    '''

    cut_height, cut_width = cut_img.shape[:2]
    pts = [[0,0], [cut_width-1,0], [cut_width-1,cut_height-1], [0,cut_height-1]]
    cut_img, rot_pts = rotate_image(cut_img, random.randint(1,359), pts)

    or_height,or_width = orig_img.shape[:2]
    cut_height, cut_width = cut_img.shape[:2]
    in_pts = np.float32([[0,0], [cut_width-1,0], [cut_width-1,cut_height-1], [0,cut_height-1]])

    if or_height*or_width<500:
        small_limit=cut_width*cut_height/10
    else:
        small_limit=150
    
    while(True):
        x_left = random.randint(0, or_width-cut_width)
        x_right = random.randint(max(0,x_left-cut_width), x_left+cut_width)
        y_up = random.randint(0, or_height-cut_height)
        y_down = random.randint(max(0,y_up-cut_height), y_up+cut_height)

        if (abs(y_up-y_down)<cut_height/10) or (abs(x_left-x_right)<cut_width/10):
            continue

        too_big = (abs(x_right-x_left)*abs(y_down-y_up)) > or_height*or_width/10
        too_small = (abs(x_right-x_left)*abs(y_down-y_up)) < small_limit
        if not (too_big or too_small):
            break

    out_pts = np.float32([[x_left,y_up],
                        [x_right,y_up],
                        [x_right,y_down],
                        [x_left,y_down]])
    M=cv2.getPerspectiveTransform(in_pts,out_pts)
    warped_image = cv2.warpPerspective(cut_img, M, (or_width, or_height))
    
    tr_pts = cv2.perspectiveTransform(rot_pts, M)

    mask = np.zeros([or_height,or_width], dtype=np.uint8)
    cv2.fillConvexPoly(mask, np.int32([tr_pts[0][0],tr_pts[1][0],tr_pts[2][0],tr_pts[3][0]]), (255, 255, 255), cv2.LINE_AA)
    orig_img[np.where(mask==255)] = warped_image[np.where(mask == 255)]

    boundbox_pts = fix_boundbox_points(tr_pts)

    return orig_img, M, boundbox_pts

def cut_mix(image, label_file, *args):
    img = image
    out_img = img.copy()
    bndboxes, points, label_data = label_file.get_objects_data()
    
    for i, bndbox in enumerate(bndboxes):
        [xmin, ymin, xmax, ymax] = bndbox

        for _ in range(random.randint(1,3)):
            out_img, mtx, boundbox_pts = image_cut_mix(img[ymin:ymax,xmin:xmax,:], out_img)
    
            label_file.add_object(label_data[i], boundbox_pts)
            #De querer hacer para COCO, habria que actualizar el codigo para que te devuelva puntos individuales y no los boundbox
    
    return out_img, label_file