import cv2
import numpy as np

mean = 0
var = 4
sigma = var**3

def noise(image, label_file, *args):

    row,col,ch= image.shape
    gauss = np.random.normal(mean,sigma,(row,col,ch))
    gauss = gauss.reshape(row,col,ch)
    noisy_img = image + gauss
    noisy_img[noisy_img > 255] = 255
    noisy_img[noisy_img < 0] = 0

    b,g,r = cv2.split(noisy_img)
    img_mrg = cv2.merge((b,g,r)) 

    return np.uint8(img_mrg), label_file