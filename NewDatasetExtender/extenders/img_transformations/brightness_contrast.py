import cv2
import numpy as np
import random

alpha = 1.0 # Simple contrast control
beta = 0    # Simple brightness control

def brightness_contrast(image, label_file, *args):
    img = image

    try:
        alpha = random.uniform(1.0,3.0)
        beta = random.randrange(20,80)
    except ValueError:
        print('Error, not a number')

    new_image = np.zeros(img.shape, img.dtype)
    # Do the operation new_image(i,j) = alpha*image(i,j) + beta
    new_image = cv2.convertScaleAbs(img, alpha=alpha, beta=beta)    

    return new_image, label_file
