import cv2
import numpy as np

def generate_random_lines(imshape,slant,drop_length):    
    drops=[]    
    for i in range(int(imshape[0]*imshape[1]//500)): ## If You want heavy rain, try increasing this        
        if slant<0:            
            x= np.random.randint(slant,imshape[1])        
        else:            
            x= np.random.randint(0,imshape[1]-slant)        
        y= np.random.randint(0,imshape[0]-drop_length)        
        drops.append((x,y))    
    return drops     

def add_rain(image):        
    imshape = image.shape
    slant_extreme=10
    slant= np.random.randint(-slant_extreme,slant_extreme)
    drop_length=20
    drop_width=2
    drop_color=(200,200,200)
    rain_drops= generate_random_lines(imshape,slant,drop_length)

    black_image = np.zeros(imshape, image.dtype)
    image = cv2.addWeighted(image, 0.7, black_image, 1, 1)
    
    for rain_drop in rain_drops:        
        cv2.line(image,(rain_drop[0],rain_drop[1]),(rain_drop[0]+slant,rain_drop[1]+drop_length),drop_color,drop_width)

    image= cv2.blur(image,(7,7))    
    return image


def rain(image, label_file, *args):

    image = add_rain(image)
    
    return image, label_file