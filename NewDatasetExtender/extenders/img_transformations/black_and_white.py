import cv2
import numpy as np

def black_and_white(image, label_file, *args):
    
    img_gry = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    img_mrg = cv2.merge((img_gry,img_gry,img_gry)) 

    return img_mrg, label_file