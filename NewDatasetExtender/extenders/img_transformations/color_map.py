import cv2

#Range [0 - 12]

def color_map(image, label_file, *args):
    color_type = args[0]
    if color_type>12:
        color_type=12
    if color_type<0:
        color_type=0

    img = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    img = cv2.applyColorMap(img, color_type)
    
    return img, label_file