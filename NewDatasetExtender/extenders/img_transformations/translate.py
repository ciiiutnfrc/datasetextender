import cv2
import numpy as np
import math
import random

def image_translation(image, boundboxes):
    '''
        This method recieves an image and returns the image
        with a random perspective resize warp
        The output image size and the shape of the data will
        be the same, but the image content will be in a smaller region
    '''
    [xmin, ymin, xmax, ymax] = boundboxes
    height,width = image.shape[:2]
    in_pts = np.float32([[0,0], [width-1,0], [width-1,height-1]])

    x_translation = random.randint(min(-int(xmin*1.2), width-int(xmax*1.2)), max(-int(xmin*1.2), width-int(xmax*1.2)))
    y_translation = random.randint(min(-int(ymin*1.2), height-int(ymax*1.2)), max(-int(ymin*1.2), height-int(ymax*1.2)))

    out_pts = np.float32([[0+x_translation, y_translation],
                        [width-1+x_translation, y_translation],
                        [width-1+x_translation, height-1+y_translation]])
                        
    M=cv2.getAffineTransform(in_pts,out_pts)
    image = cv2.warpAffine(image, M, (width, height))
    return image, M

def fix_points(pts, rotation_mat):
    """
        Given a matrix, fixes the boundbox points
    """

    xmin, xmax, ymin, ymax = pts[0], pts[1], pts[2], pts[3]
    tr_pts = np.float32([[xmin,ymin],[xmax,ymin],[xmin,ymax],[xmax,ymax]]).reshape(-1,1,2)

    rotated_points = cv2.transform(tr_pts, rotation_mat)

    out_min_x=math.inf
    out_max_x=-1
    out_min_y=math.inf
    out_max_y=-1

    for point in rotated_points:
        if point[0][0]>out_max_x:
            out_max_x=point[0][0]
        if point[0][0]<out_min_x:
            out_min_x=point[0][0]

        if point[0][1]>out_max_y:
            out_max_y=point[0][1]
        if point[0][1]<out_min_y:
            out_min_y=point[0][1]

    return int(out_min_x), int(out_max_x), int(out_min_y), int(out_max_y)


def translate(image, label_file, *args):

    [width, height] = label_file.get_image_data()[:2]
    xmax_temp = 0
    xmin_temp = width
    ymax_temp = 0
    ymin_temp = height

    bndboxes, points, label_data = label_file.get_objects_data()
    for bndbox in bndboxes:
        [xmin, ymin, xmax, ymax] = bndbox

        if xmin < xmin_temp :
            xmin_temp = xmin
        if xmax > xmax_temp :
            xmax_temp = xmax
        if ymin < ymin_temp :
            ymin_temp = ymin
        if ymax > ymax_temp :
            ymax_temp = ymax

    img, mtx = image_translation(image, [xmin_temp, ymin_temp, xmax_temp, ymax_temp])

    label_file.remove_all_objects()
    for i, bndbox in enumerate(bndboxes):
        [xmin, ymin, xmax, ymax] = bndbox

        n_xmin, n_xmax, n_ymin, n_ymax = fix_points([xmin,xmax,ymin,ymax], mtx)

        n_ymin = min(height,max(0,n_ymin))
        n_ymax = min(height,max(0,n_ymax))
        n_xmin = min(width,max(0,n_xmin))
        n_xmax = min(width,max(0,n_xmax))

        label_file.add_object(label_data[i], [n_xmin,n_ymin, n_xmax,n_ymax])
    
    return img, label_file