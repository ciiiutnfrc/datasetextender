import cv2
import numpy as np

def rotate_image(mat, angle):
    """
        Rotates an image (angle in degrees) and expands image to avoid cropping
    """

    height, width = mat.shape[:2] # image shape has 3 dimensions
    image_center = (width/2, height/2) # getRotationMatrix2D needs coordinates in reverse order (width, height) compared to shape

    rotation_mat = cv2.getRotationMatrix2D(image_center, angle, 1.)

    # rotation calculates the cos and sin, taking absolutes of those.
    abs_cos = abs(rotation_mat[0,0]) 
    abs_sin = abs(rotation_mat[0,1])

    # find the new width and height bounds
    bound_w = int(height * abs_sin + width * abs_cos)
    bound_h = int(height * abs_cos + width * abs_sin)

    # subtract old image center (bringing image back to origo) and adding the new image center coordinates
    rotation_mat[0, 2] += bound_w/2 - image_center[0]
    rotation_mat[1, 2] += bound_h/2 - image_center[1]

    # rotate image with the new bounds and translated rotation matrix
    rotated_mat = cv2.warpAffine(mat, rotation_mat, (bound_w, bound_h))
    return rotated_mat, rotation_mat, bound_w, bound_h


def rotate_points(pts, rotation_mat):
    """
        Given a rotation matrix, rotates points in the image
    """

    xmin, xmax, ymin, ymax = pts[0], pts[1], pts[2], pts[3]
    tr_pts = np.float32([[xmin,ymin],[xmax,ymin],[xmin,ymax],[xmax,ymax]]).reshape(-1,1,2)

    rotated_points = cv2.transform(tr_pts, rotation_mat)

    out_min_x=rotated_points[0][0][0]
    out_max_x=-1
    out_min_y=rotated_points[0][0][1]
    out_max_y=-1

    for point in rotated_points:
        if point[0][0]>out_max_x:
            out_max_x=point[0][0]
        if point[0][0]<out_min_x:
            out_min_x=point[0][0]

        if point[0][1]>out_max_y:
            out_max_y=point[0][1]
        if point[0][1]<out_min_y:
            out_min_y=point[0][1]

    return int(out_min_x), int(out_max_x), int(out_min_y), int(out_max_y)

def rotate(image, label_file, *args):
    ang = round(args[0],3)
    img = image
    
    img, mtx, new_width, new_height = rotate_image(img, ang)

    bndboxes, points, label_data = label_file.get_objects_data()
    label_file.remove_all_objects()

    for i, bndbox in enumerate(bndboxes):
        [xmin, ymin, xmax, ymax] = bndbox

        n_xmin, n_xmax, n_ymin, n_ymax = rotate_points([xmin,xmax,ymin,ymax], mtx)

        label_file.add_object(label_data[i], [n_xmin,n_ymin, n_xmax,n_ymax])

    
    label_file.set_image_data(new_width, new_height)

    return img, label_file