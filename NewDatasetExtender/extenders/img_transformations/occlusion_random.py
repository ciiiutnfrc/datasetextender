import cv2
import numpy as np
import random

def rand_noise_box(image, minpts=None, maxpts=None):
    '''
        This method recieves an image, and the min and max points
        where the box could be placed (for example, the range of the
        bound-box label), and returns an image with random occlusion
        on the interest point.
        If no points are given, the range will be the whole image.
    '''
    row,col,ch= image.shape
    mean = -0.5
    var = 1.5
    sigma = var**3
    gauss = np.random.normal(mean,sigma,(row,col,ch))
    cv2.resize(gauss, (col, row))

    blackimg = np.zeros([row,col,ch],dtype=np.uint8)
    grayimg = np.zeros([row,col,ch],dtype=np.uint8)
    grayimg.fill(126)
    noisy = cv2.addWeighted(blackimg, 0.0001, gauss, 0.99, 0.1, dtype=cv2.CV_32F)

    gray = cv2.cvtColor(noisy, cv2.COLOR_BGR2GRAY)
    gray_three = cv2.merge([gray,gray,gray])

    if minpts is None:
        [minx, miny] = [0,0]
    else:
        [minx, miny] = minpts
    if maxpts is None:
        [maxx, maxy] = [col-1,row-1]
    else:
        [maxx, maxy] = maxpts

    rndpts = [[random.randint(minx,maxx),random.randint(miny,maxy)],
            [random.randint(minx,maxx),random.randint(miny,maxy)]]
    rndpts = [[rndpts[0][0],rndpts[0][1]],
            [rndpts[1][0],rndpts[0][1]],
            [rndpts[1][0],rndpts[1][1]],
            [rndpts[0][0],rndpts[1][1]]]

    mask = np.zeros([row,col], dtype=np.uint8)
    cv2.fillConvexPoly(mask, np.int32([rndpts]), (255, 255, 255), cv2.LINE_AA)
    
    rand_box={0:blackimg,1:grayimg,2:noisy,3:gray_three}

    image[np.where(mask == 255)] = rand_box[random.randint(0,3)][np.where(mask == 255)]

    return image

def occlusion_random(image, label_file, *args):

    img = image

    [width, height] = label_file.get_image_data()[:2]
    bndboxes, points, label_data = label_file.get_objects_data()
    
    for i, bndbox in enumerate(bndboxes):
        [xmin_temp, ymin_temp, xmax_temp, ymax_temp] = bndbox

        minpts=[max(0, int(xmin_temp*random.uniform(0.9,1))),max(0, int(ymin_temp*random.uniform(0.9,1)))]
        maxpts=[min(width-1, int(xmax_temp*random.uniform(1,1.1))), min(height-1, int(ymax_temp*random.uniform(1,1.1)))]

        img = rand_noise_box(img, minpts, maxpts)
    
    return img, label_file