import cv2

#Recommended range [3 - 7]

def mosaic(image, label_file, *args):
    mosaic_size = args[0]

    size = image.shape[:2][::-1]

    img_tmp = cv2.resize(image, (int(size[0]/mosaic_size), int(size[1]/mosaic_size)))

    img_dst = cv2.resize(img_tmp, size, interpolation=cv2.INTER_NEAREST)

    return img_dst, label_file