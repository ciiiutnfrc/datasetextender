import cv2
import numpy as np

def resize_image(image, new_size):
    '''
        This method recieves an image and returns a new
        image resized, and the matrix transformation
    '''
    (h, w) = new_size
    height,width,ch= image.shape
    in_pts = np.float32([[0,0], [width-1,0], [width-1,height-1], [0,height-1]])
    out_pts = np.float32([[0,0], [w-1,0], [w-1,h-1], [0,h-1]])

    M=cv2.getPerspectiveTransform(in_pts,out_pts)
    img = cv2.warpPerspective(image, M, (w,h))
    return img, M

def fix_points(pts, rotation_mat):
    """
        Given a rotation matrix, rotates points in the image
    """

    xmin, xmax, ymin, ymax = pts[0], pts[1], pts[2], pts[3]
    tr_pts = np.float32([[xmin,ymin],[xmax,ymin],[xmin,ymax],[xmax,ymax]]).reshape(-1,1,2)

    rotated_points = cv2.perspectiveTransform(tr_pts, rotation_mat)

    out_min_x=rotated_points[0][0][0]
    out_max_x=-1
    out_min_y=rotated_points[0][0][1]
    out_max_y=-1

    for point in rotated_points:
        if point[0][0]>out_max_x:
            out_max_x=point[0][0]
        if point[0][0]<out_min_x:
            out_min_x=point[0][0]

        if point[0][1]>out_max_y:
            out_max_y=point[0][1]
        if point[0][1]<out_min_y:
            out_min_y=point[0][1]

    return int(out_min_x), int(out_max_x), int(out_min_y), int(out_max_y)

def mixup(image, label_file, *args):
    image2 = args[1]
    label_file2 = args[2]

    img1 = image
    img2 = image2

    #  We need to get the matrix in case the images shapes are not the same, in order to 
    # relocate the boundbox points
    img2, mtx = resize_image(img2, img1.shape[:2])
    img_out = cv2.addWeighted(img1, 0.5, img2, 0.5, 0)

    #img, mtx = image_perspective(img)
    
    bndboxes, points, label_data2 = label_file2.get_objects_data()

    for i, bndbox in enumerate(bndboxes):
        [xmin, ymin, xmax, ymax] = bndbox

        n_xmin, n_xmax, n_ymin, n_ymax = fix_points([xmin,xmax,ymin,ymax], mtx)

        label_file.add_object(label_data2[i], [n_xmin,n_ymin,n_xmax,n_ymax])
    
    return img_out, label_file